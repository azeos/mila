/*** Token ***/
var token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': token
    }
});

/*** Fade in alerts, etc ***/
if ($('.alert.fade').length) {
    setTimeout("$('.alert.fade:not(.ajax-errors)').addClass('show')", 500);
    setTimeout("$('.alert.fade:not(.alert-dismissible, .ajax-errors)').alert('close')", 3500);
}

/*** Tooltips ***/
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});

/*** Sticky ***/
$('.sticky').sticky({
    widthFromWrapper: false,
    zIndex: 1020
});