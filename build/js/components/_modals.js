// Modal remote
$('body').on('click', '[data-toggle="modal"][data-remote], [data-toggle="modal"][data-target][href]', function (e) {
    e.stopPropagation(); // Previene que se abra el modal
    e.preventDefault(); // Previene que se cargue la URL

    // Carga el contenido del modal
    var $this = $(this);
    var $modal = $this.data('target') ? $($this.data('target')) : $($this.attr('href'));
    var url = $this.data('remote') || $this.attr('href');
    $modal.find('.modal-content').load(url, function () {
        $modal.trigger('loaded.bs.modal');
        $modal.modal('show');
    });
});

// Borra el contenido del modal al cerrarlo
$('body').on('hidden.bs.modal', '.modal-remote', function () {
    $(this).find('.modal-content').html('');
});