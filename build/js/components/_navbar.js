/*** Main navbar ***/
// Crea un backdrop
$(document.body).append('<div class="navbar-backdrop modal-backdrop fade"></div>');

// Apertura del navbar en mobile
function openNavbarMobile(e) {
    // Si no está abierto y es mobile
    if (!$('html').hasClass('main-navbar-open') && ($(window).width() < 768 || $('html').hasClass('main-navbar-overlay'))) {
        e = e || false;
        $('html').addClass('main-navbar-open');

        // Si se clickeo el toggler detiene la propagación para que no se ejecute el "click.body"
        if (e) {
            e.stopPropagation();
        }

        // Muestra un backdrop
        $('.navbar-backdrop').addClass('show');

        // Se cierra el navbar al clickear en cualquier lado (menos en el menú)
        $('.main-navbar').on('click.mainNavbar', function (e) {
            e.stopPropagation();
        });
        // Click body
        $(document.body).on('click.body', function () {
            closeNavbarMobile();
        });

        // Trigger
        $('html').trigger('main-navbar.opened');
    }
}

// Cierre del navbar en mobile
function closeNavbarMobile() {
    // Si está abierto
    if ($('html').hasClass('main-navbar-open')) {
        // Cierra el navbar
        $('.main-navbar-open').removeClass('main-navbar-open');

        // Oculta el backdrop
        $('.navbar-backdrop').removeClass('show');

        // Apaga los eventos
        $(document.body).off('click.body');
        $('.main-navbar').off('click.mainNavbar');

        // Trigger
        $('html').trigger('mobile-navbar.closed');
    }
}

// Abre o cierra el navbar
function toggleNavbarMobile(e) {
    if ($('html').hasClass('mobile-navbar-open')) {
        closeNavbarMobile();
    } else {
        openNavbarMobile(e);
    }
}

// Toggler
$('.main-navbar-toggler').click(function (e) {
    if ($(window).width() > 767 && ! $('html').hasClass('main-navbar-overlay')) {
        if ($('html').hasClass('main-navbar-collapse')) {
            $('html').removeClass('main-navbar-collapse').trigger('desktop-navbar.opened');
        } else {
            $('html').addClass('main-navbar-collapse').trigger('desktop-navbar.closed');
        }
    } else {
        toggleNavbarMobile(e);
    }
});

// Swipe
// Hace el bind solo en touchstart para asegurarse que sea un dispositivo touch
// sino esto bloquea la selección de texto con mouse
$(document).on('touchstart', function () {
    $(document.body).swipe({
        // Si se realiza un swipe fuera de un input le saca el foco
        swipeStatus: function (event, phase) {
            if (phase == 'move' || phase == 'start') {
                var $target = event.target.nodeName;
                if ($target.toLowerCase() === 'input' || $(event.target.parentNode).is('[class*="selectize"')) {
                    return false;
                } else {
                    $('input').blur();
                }
            }
        },
        swipeRight: function (event, direction, distance, duration, fingerCount, fingerData) {
            if (fingerData[0].start.x < 15) {
                openNavbarMobile();
            }
        },
        swipeLeft: function () {
            closeNavbarMobile();
        },
        excludedElements: $.fn.swipe.defaults.excludedElements + ', button, input, select, textarea, a'
    });
});