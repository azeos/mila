// Bootbox
$('body').on('click', '.btn-delete', function (e) {
    // Previene el click
    e.preventDefault();

    // Formulario
    var $btn = $(this);
    var $form = $btn.closest('form');
    var $method = $form.find('input[name="_method"]');
    var value = this.value;

    // Confirm
    bootbox.dialog({
        message: $btn.data('bbox-msg'),
        buttons: {
            main: {
                label: 'Cancelar',
            },
            success: {
                label: 'Aceptar',
                className: 'btn-danger',
                callback: function () {
                    // Si ya existe un input _method le cambia el value y guarda el valor original en "data-original"
                    if ($method.length) {
                        $method.data('original', $method.val()).val(value);
                    }
                    // Sino crea uno nuevo
                    else {
                        var $input = $('<input>', {type: 'hidden', name: $btn.attr('name'), value: value});
                        $form.append($input);
                    }

                    // Envía el formulario
                    $form.submit();
                }
            }
        }
    });
});

// Stateful button
var buttonState = function ($btn, state) {
    var val = $btn.html();

    // Loading text
    if ($btn.data('loadingText') == null) {
        $btn.data('loadingText', '<i class="fas fa-spinner fa-pulse"></i>');
    }

    // Reset text
    if ($btn.data('resetText') == null) {
        $btn.data('resetText', val);
    }

    // Reset class
    if ($btn.data('resetClass') == null) {
        // Revisa si tiene alguna de las clases por defecto de Bootstrap
        // https://getbootstrap.com/docs/4.1/components/buttons/#examples
        var resetClass = $btn.attr('class').match(/btn-(?:outline-)?(?:primary|secondary|success|danger|warning|info|light|dark|link)/i);
        if (resetClass) {
            $btn.data('resetClass', resetClass[0]);
        }
    }

    // Clase del estado anterior
    if ($btn.data('prevClass') == null) {
        $btn.data('prevClass', $btn.data('resetClass'));
    }

    // Texto y clase que se va a utilizar
    var stateText = state + 'Text';
    stateText = $btn.data(stateText) ? stateText : 'resetText';
    var stateClass = state + 'Class';
    stateClass = $btn.data(stateClass) ? stateClass : 'resetClass';

    // Texto del estado correspondiente, si no existe se usa el del reset
    $btn.html($btn.data(stateText));

    // Si la clase nueva y la anterior son distintas
    if ($btn.data(stateClass) != $btn.data('prevClass')) {
        // Saca la clase anterior
        $btn.removeClass($btn.data('prevClass'));

        // Agrega la clase nueva
        $btn.addClass($btn.data(stateClass));
        $btn.data('prevClass', $btn.data(stateClass));
    }

    if (stateText == 'loadingText') {
        $btn.addClass('disabled').attr('disabled', 'disabled').prop('disabled', true);
    } else if ($btn.hasClass('disabled')) {
        $btn.removeClass('disabled').removeAttr('disabled').prop('disabled', false);
    }
};

// Submit automático para filtros
$('.autosubmit-filter').change(function () {
    $(this.form).submit();
});

// Fileinput FIX Fx 45
$('.fileinput').on('clear.bs.fileinput', function (e) {
    $(e.target).find('.btn-file input[type=hidden]').val('delete');
});

// AJAX form
$('body').on('submit', '.form-ajax', function (e) {
    // Previene el envío
    e.preventDefault();

    // Estado del botón de submit
    // Si method == DELETE, selecciona el btn-delete
    var $method = $(this).find('input[name="_method"]');
    if ($method.length && $method.val() == 'DELETE') {
        var $btn = $(this).find('.btn-delete');
    } else {
        var $btn = $(this).find('.btn-state');
    }
    buttonState($btn, 'loading');

    // Formulario
    var $form = $(this);
    var action = $form.attr('action');
    var settings = {
        method: 'POST',
        url: action,
        dataType: 'json',
        cache: false
    };

    // Sí el formulario tiene archivos
    if ($form.attr('enctype')) {
        // Sí el explorador no soporta FormData se envía el formulario sin AJAX
        // http://www.hydrogen18.com/blog/fixing-what-isnt-broke.html#
        // https://bugzilla.mozilla.org/show_bug.cgi?id=1250148
        if (window.FormData === undefined) {
            $form.removeClass('form-ajax').submit();
            return true;
        }
        var data = new FormData(this);
        settings['contentType'] = false;
        settings['processData'] = false;
    } else {
        var data = $form.serialize();
    }
    settings['data'] = data;

    // Envío
    $.ajax(settings).done(function (response) {
        // Si response es un string
        if (typeof response == 'string') {
            // Redirecciona a la URL correspondiente
            var oldUrl = location.href.replace(location.hash, '');
            window.location = response;

            // Si lo único que cambió fue el hash, fuerza un reload
            if (oldUrl == response.replace(location.hash, '')) {
                location.reload();
            }
        } else {
            // Success del botón de submit
            buttonState($btn, 'success');

            // Custom event
            $form.trigger('form.ajax.success', response);
        }
    }).fail(function (response) {
        // Errores
        var responseJSON = response.responseJSON;

        if (responseJSON) {
            if ('errors' in responseJSON) {
                // Muestra los errores
                var html = '<ul>';
                $.each(responseJSON.errors, function (field, errors) {
                    $.each(errors, function (key, message) {
                        html += '<li>' + message + '</li>';
                    });
                });
                html += '</ul>';
            } else if ('flash' in responseJSON) {
                // Muestra los errores
                var html = '<ul>';
                $.each(responseJSON.flash, function (key, error) {
                    html += '<li>' + error.message + '</li>';
                });
                html += '</ul>';
            } else {
                // Muestra el error
                html = responseJSON.message;
            }
        } else {
            // Muestra el error
            html = response.statusText;
        }

        // Muestra los errores
        if ($form.hasClass('form-modal')) {
            $form.find('.ajax-errors').html(html).addClass('show');
        } else {
            $('.ajax-errors').html(html).addClass('show');
        }

        // Fail del botón de submit
        if (response.status == 422) {
            buttonState($btn, 'warning');
        } else {
            buttonState($btn, 'fail');
        }

        // Resetea el _method
        var $method = $form.find('input[name="_method"]');
        if (typeof $method.data('original') !== 'undefined') {
            $method.val($method.data('original'));
            $method.removeData('original');
        }
    });
});

/*** Sortable ***/
var createSort = function () {
    var items = $(this).sortable('option', 'items');
    $(items, this).each(function (i) {
        $(this).attr('data-initpos', i);
    });
};
var fixHelperModified = function (parent, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();
    $helper.children().each(function (index) {
        $(this).width($originals.eq(index).width())
    });
    return $helper;
};
$('.sortable tbody, .row.sortable').sortable({
    revert: 150,
    create: createSort,
    cancel: 'a,input,textarea,button,select,option',
    helper: fixHelperModified,
    forcePlaceholderSize: true,
    placeholder: 'sortable-highlight',
    handle: '.sortable-handle',
    create: function () {
        // Si es una tabla limita el movimiento de manera vertical
        var axis = $(this).is('.sortable tbody') ? 'y' : false;
        $(this).sortable('option', 'axis', axis);
    },
    start: function (e, ui) {
        // Thumbnails placeholder
        if (ui.item.children('.card').length) {
            // Thumbnail
            $thumb = ui.helper.children('.card');

            // Copia las classes de columnas de Bootstrap
            colClasses = '';
            parentClasses = ui.item.attr('class').split(' ');
            $(parentClasses).each(function (i, e) {
                if (e.match('^col-')) {
                    colClasses += ' ' + e;
                }
            });

            // Placeholder
            ui.placeholder.addClass(colClasses).append('<div class="card" style="height:' + $thumb.innerHeight() + 'px;"/>');
        }
    },
    update: function () {
        if ($(this).closest('.sortable').hasClass('sortable-ajax')) {
            var $form = $(this).closest('.sortable-form');
            $.post($form.attr('action'), $form.serialize());
        }
    }
});

/*** Dropzone.js ***/
// Oculta una foto
$('body').on('click', '.btn-hide', function (e, trigger) {
    // Options
    var $this = $(this);
    var showText = (typeof $this.data('showText') !== 'undefined') ? $this.data('showText') : 'Mostrar';
    var hideText = (typeof $this.data('hideText') !== 'undefined') ? $this.data('hideText') : 'Ocultar';

    // Intercambia los íconos y los textos
    if ($this.find('.btn-hide-icon').hasClass('fa-eye-slash')) {
        $this.find('.btn-hide-icon').removeClass('fa-eye-slash').addClass('fa-eye');
        $this.find('.foto-hide').val(1);
        $this.closest('.card').addClass('deleted');
        $this.attr('title', showText);
    } else {
        $this.find('.btn-hide-icon').removeClass('fa-eye').addClass('fa-eye-slash');
        $this.find('.foto-hide').val(0);
        $this.closest('.card').removeClass('deleted');
        $this.attr('title', hideText);
    }

    // Muestra el tooltip
    $this.tooltip('dispose');
    if (typeof trigger == 'undefined') {
        $this.tooltip('show');
    } else {
        $this.tooltip('enable');
    }
});

// Fotos
$('.dropzone-upload').each(function () {
    // Options
    var $this = $(this);
    var url = $this.is('form') ? $this.attr('action') : $this.data('url');
    var autoProcessQueue = (typeof $this.data('autoProcessQueue') !== 'undefined') ? $this.data('autoProcessQueue') : true;
    var maxFilesize = (typeof $this.data('maxFilesize') !== 'undefined') ? $this.data('maxFilesize') : 2;
    var maxFiles = (typeof $this.data('maxFiles') !== 'undefined') ? $this.data('maxFiles') : null;
    var paramName = (typeof $this.data('paramName') !== 'undefined') ? $this.data('paramName') : 'image';
    var acceptedFiles = (typeof $this.data('acceptedFiles') !== 'undefined') ? $this.data('acceptedFiles') : 'image/*';
    var idField = (typeof $this.data('idField') !== 'undefined') ? $this.data('idField') : 'id';
    var urlField = (typeof $this.data('urlField') !== 'undefined') ? $this.data('urlField') : 'imagenUrl';
    var hiddenField = (typeof $this.data('hiddenField') !== 'undefined') ? $this.data('hiddenField') : 'deleted_at';

    // Copia el HTML del template y lo saca del DOM
    var $template = $this.find('.dropzone-template');
    var previewThumbnails = $template.html();
    $template.html('');

    $this.dropzone({
        url: url,
        params: {
            _token: token
        },
        method: 'post',
        autoProcessQueue: autoProcessQueue,
        maxFilesize: maxFilesize,
        maxFiles: maxFiles,
        paramName: paramName,
        acceptedFiles: acceptedFiles,
        addRemoveLinks: false,
        thumbnailWidth: null,
        thumbnailHeight: null,
        previewTemplate: previewThumbnails,
        previewsContainer: $this.find('.dropzone-template')[0],
        clickable: $this.find('.btn-addFile')[0],
        init: function () {
            // Tooltips
            this.on('addedfile', function (file) {
                $(file.previewTemplate).find('[data-toggle="tooltip"]').tooltip();
            });
            this.on('removedfile', function (file) {
                $(file.previewElement).find('[data-toggle="tooltip"]').tooltip('dispose');
            });

            // Error
            this.on('error', function (file, message) {
                bootbox.alert(message);
                this.removeFile(file);
            });

            // Muestra las fotos ya subidas
            var fotos = $(this.element).data('fotos');
            if (typeof fotos == 'object') {
                var thisDropzone = this;
                $.each(fotos, function (i, foto) { //loop through it
                    var mockFile = {id: foto[idField], url: foto[urlField], accepted: true}; // here we get the file name and size as response
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit('addedfile', mockFile);
                    thisDropzone.emit('thumbnail', mockFile, mockFile.url);
                    thisDropzone.emit('complete', mockFile);
                    thisDropzone._updateMaxFilesReachedClass();
                    var $thumb = $(thisDropzone.previewsContainer).find('.card').last();
                    // Input name
                    $thumb.find('[name^="fotos[fotoId]"]').each(function () {
                        var $this = $(this);
                        $this.attr('name', $this.attr('name').replace('fotoId', foto[idField]));
                        // Datos
                        // Ej1: 'data-value' => '[{"array":"translations", "value":"datos"}]'
                        // Ej2: 'data-value' => 'imagen_resize'
                        if ($this.data('value')) {
                            var campo = $this.data('value');
                            if ($.isArray(campo)) {
                                var array = campo[0]['array'];
                                var campo = campo[0]['value'];
                                var locale = $this.attr('lang');
                                $.each(foto[array], function (i, e) {
                                    if (e.locale == locale) {
                                        $this.val(e[campo]);
                                    }
                                }, campo);
                            } else {
                                $this.val(foto[campo]);
                            }
                        }
                        // Ej: 'data-readonly' => 'slideable_id'
                        if ($this.data('readonly')) {
                            var campo = $this.data('readonly');
                            if (foto[campo]) {
                                $this.prop('readonly', true).attr('readonly', true);
                            }
                        }
                    });
                    // Hide
                    if (foto[hiddenField]) {
                        $thumb.find('.btn-hide').trigger('click', [true]);
                    }
                    // Tab ID
                    $thumb.find('[href^="#fotoId"]').each(function () {
                        var $this = $(this);
                        $this.attr('href', $this.attr('href').replace('fotoId', 'tab-' + foto[idField]));
                    });
                    $thumb.find('[id^="fotoId"]').each(function () {
                        var $this = $(this);
                        $this.attr('id', $this.attr('id').replace('fotoId', 'tab-' + foto[idField]));
                    });
                });
            }
        },
        sending: function (file) {
            $(file.previewTemplate).find('.progress').removeClass('d-none');
        },
        success: function (file, response) {
            // Input name
            $(file.previewTemplate).find('[name^="fotos[fotoId]"]').each(function () {
                $(this).attr('name', $(this).attr('name').replace('fotoId', response));
            });
            // Tab ID
            $(file.previewTemplate).find('[href^="#fotoId"]').each(function () {
                $(this).attr('href', $(this).attr('href').replace('fotoId', 'tab-' + response));
            });
            $(file.previewTemplate).find('[id^="fotoId"]').each(function () {
                $(this).attr('id', $(this).attr('id').replace('fotoId', 'tab-' + response));
            });
            $(file.previewTemplate).find('.progress').addClass('d-none');
        }
    });
});