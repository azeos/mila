// AJAX Pagination
$('body').on('click', '.ajax-pagination .pagination a, .ajax-pagination .pager a', function (e){
    e.preventDefault();
    var url = $(this).attr('href');
    var $target = $(this).closest('.ajax-pagination');
    $target.load(url);
});

// DataTables
$.fn.dataTable.moment('DD/MM/YYYY', 'es');
$.extend(true, $.fn.dataTable.defaults, {
    order: [],
    columnDefs: [
        {targets: 'no-sort', orderable: false},
        {targets: 'no-search', searchable: false},
    ],
    dom:
        "<'row'<'col-sm-12 col-md-6 mb-3'l><'col-sm-12 col-md-6 mb-3'f>>" +
        "<'row'<'col-sm-12'<'card rounded-0'<'table-responsive'tr>>>>" +
        "<'row'<'col-sm-12 col-md-5 mt-3'i><'col-sm-12 col-md-7 mt-3'p>>",
    initComplete: function () {
        // Busca custom filters y los mueve a la barra de filtros
        var id = $(this).attr('id');
        if (id && $(this).hasClass('has-custom-filters')) {
            $('.dataTables-custom-filters[data-table="' + id + '"]').children().prependTo('#' + id + '_filter');
        }
    },
    language: {
        processing: 'Procesando...',
        lengthMenu: 'Mostrar _MENU_ registros',
        zeroRecords: 'No se encontraron resultados',
        emptyTable: 'Ningún dato disponible en esta tabla',
        info: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
        infoEmpty: 'Mostrando registros del 0 al 0 de un total de 0 registros',
        infoFiltered: '(filtrado de un total de _MAX_ registros)',
        infoPostFix: '',
        search: '_INPUT_',
        searchPlaceholder: 'Buscar...',
        url: '',
        infoThousands: ',',
        loadingRecords: 'Cargando...',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: '»',
            previous: '«'
        },
        aria: {
            sortAscending: ': Activar para ordenar la columna de manera ascendente',
            sortDescending: ': Activar para ordenar la columna de manera descendente'
        }
    }
});

$('.dataTable').DataTable();

// Search accents
$('body').on('keyup', '.dataTables_filter [type="search"]', function () {
    var $table = $(this).closest('.dataTables_wrapper').find('table').DataTable();
    $table.search($.fn.DataTable.ext.type.search.string(this.value)).draw();
});

// Search columns
$('body').on('change', '[data-column-filter]', function () {
    var $this = $(this);
    var $table = $this.data('table') ? $('#' + $this.data('table')).DataTable() : $this.closest('.dataTables_wrapper').find('table').DataTable();
    var column = $this.data('columnFilter');
    $table.column(column).search($.fn.DataTable.ext.type.search.string(this.value)).draw();
});