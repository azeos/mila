// Selectize
function selectize($selector) {
    $selector.selectize({
        onChange: function (value) {
            var input = this.$input[0]; // Input original
            var chained = $(input).data('chained'); // Input con el que está encadenado

            // Si está encadenado
            if (chained && $(chained).length) {
                var dataValue = $(input).data('chainedValue'); // Valor que debe tener el input para habilitar el encadenado
                var isValue = false; // Si dataValue es igual -o contiene- a value
                var dataUrl = $(input).data('chainedUrl'); // URL que se debe llamar para llenar el input
                var getUrl = true // Si hay que cargar la URL

                // data-chained-value
                if (dataValue) {
                    // Si dataValue es un array, revisa si el valor está presente
                    if (typeof dataValue == 'object') {
                        if (dataValue.indexOf(value) > -1) {
                            isValue = true;
                        }
                    } else if (value == dataValue) {
                        isValue = true;
                    }

                    // La URL solo se tiene que cargar si isValue es true, así que se le asigna el mismo valor
                    getUrl = isValue;

                    // Si isValue es true y no hay URL, éste bloque se encarga del selectize. Sino lo hace el de la URL.
                    if (isValue && ! dataUrl) {
                        $(chained)[0].selectize.enable();
                    } else {
                        $(chained)[0].selectize.disable();
                        $(chained)[0].selectize.clear();
                    }
                }

                // data-chained-url
                if (dataUrl && getUrl) {
                    $(chained)[0].selectize.load(function (callback) {
                        if (value) {
                            $.get(dataUrl + value, function (data) {
                                $(chained)[0].selectize.enable();
                                $(chained)[0].selectize.clear();
                                $(chained)[0].selectize.clearOptions();
                                callback(data);
                            });
                        } else {
                            $(chained)[0].selectize.disable();
                            $(chained)[0].selectize.clear();
                        }
                    });
                }

                // Si no hay dataValue ni dataUrl, solo se chequea si hay value
                if ( ! dataValue && ! dataUrl) {
                    if (value) {
                        $(chained)[0].selectize.enable();
                    } else {
                        $(chained)[0].selectize.disable();
                        $(chained)[0].selectize.clear();
                    }
                }
            }
        }
    });
}
selectize($('.selectize:not([multiple])'));

function selectizeMultiple($selector) {
    $selector.selectize({
        plugins: ['remove_button'],
        persist: false,
        maxItems: null
    });
}
selectizeMultiple($('.selectize[multiple]'));

function selectizeOrder($selector) {
    $selector.selectize({
        plugins: ['remove_button', 'drag_drop'],
        persist: false,
        maxItems: null
    });
}
selectizeOrder($('.selectize-order'));

// Autofocus fix
var input = $('.selectized[autofocus]');
if (input.length) {
    input[0].selectize.focus();
}

// Inicia los select al cargar un modal o un tab por AJAX
$('body').on('loaded.bs.modal loaded.bs.tab', '.has-selectize', function (e) {
    if (e.namespace == 'bs.modal') {
        var $this = $(this);
    } else {
        var $this = $($(this).data('target'));
    }

    selectize($this.find('.selectize:not([multiple])'));
    selectizeMultiple($this.find('.selectize[multiple]'));
    selectizeOrder($this.find('.selectize-order'));
});