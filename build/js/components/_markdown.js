// Markdown
$('.markdown').each(function () {
    var $this = $(this);

    var easymde = new EasyMDE({
        element: this,
        spellChecker: false,
        forceSync: true,
    });

    // Si se hace focus en el textarea original cambia al del markdown
    $this.on('focus', function () {
        $this.blur();
        $(easymde.codemirror.getInputField()).focus();
    });

    // Si el textarea original es required
    if ($this.is('[required]')) {
        // Lo vuelve a hacer visible
        $this.addClass('markdown-required').css('display', '');

        // Si se produce un error, agrega un margin-top al textarea teniendo en cuenta
        // el alto del toolbar así el mensaje de validación se ubica correctamente
        $this.on('invalid', function () {
            var $editor = $this.next('.editor-toolbar');
            if ($editor.length) {
                $this.css('marginTop', $editor.outerHeight() + 10);
            }
        });
    }

    // Actualiza el markdown cuando se muestra el tab en el que se encuentra
    $('a[data-toggle="tab"].has-markdown').on('shown.bs.tab', function () {
        easymde.codemirror.refresh();
    });
});