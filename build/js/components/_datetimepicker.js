// Datepicker
flatpickr.localize(flatpickr.l10ns.es);
flatpickr.setDefaults({
    dateFormat: 'd/m/Y',
    time_24hr: true,
});
$('.datepicker').flatpickr();
$('.datetimepicker').flatpickr({
    dateFormat: 'd/m/Y H:i',
    enableTime: true,
});

// Linked pickers
function datepickersLinked(e) {
    var $picker = $(e);
    var $linked = $('#' + $picker.data('linked'));

    // Valores iniciales
    if ($picker.val()) {
        $linked[0]._flatpickr.set('minDate', $picker.val());
    }
    if ($linked.val()) {
        $picker[0]._flatpickr.set('maxDate', $linked.val());
    }

    // Actualiza los valores onChange()
    $picker[0]._flatpickr.set('onChange', function (selectedDates, dateStr) {
        $linked[0]._flatpickr.set('minDate', dateStr);
    });
    $linked[0]._flatpickr.set('onChange', function (selectedDates, dateStr) {
        $picker[0]._flatpickr.set('maxDate', dateStr);
    });
}
$('.datepicker[data-linked], .datetimepicker[data-linked]').each(function () {
    datepickersLinked(this);
});

// Inicia los datepickers cuando se abre el modal
$('body').on('shown.bs.modal', '.modal.has-datepicker', function () {
    $(this).find('.datepicker').flatpickr({
        static: true, // https://github.com/flatpickr/flatpickr/issues/1352
    });
    $(this).find('.datetimepicker').flatpickr({
        dateFormat: 'd/m/Y H:i',
        enableTime: true,
        static: true, // https://github.com/flatpickr/flatpickr/issues/1352
    });
    $(this).find('.datepicker[data-linked], .datetimepicker[data-linked]').each(function () {
        datepickersLinked(this);
    });
});