let mix = require('laravel-mix');
let path = require('path');

var publicPath = path.normalize('dist');
mix.setPublicPath(publicPath);

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
        'build/js/vendor/jquery-ui.js',
        'build/js/vendor/jquery.ui.touch-punch.min.js',
        'node_modules/moment/min/moment.min.js',
        'node_modules/moment/locale/es.js',
        'node_modules/bootbox/dist/bootbox.min.js',
        'node_modules/datatables.net/js/jquery.dataTables.min.js',
        'node_modules/datatables.net-plugins/filtering/type-based/accent-neutralise.js',
        'node_modules/datatables.net-plugins/sorting/datetime-moment.js',
        'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
        'node_modules/dropzone/dist/dropzone.js',
        'node_modules/easymde/dist/easymde.min.js',
        'node_modules/flatpickr/dist/flatpickr.min.js',
        'node_modules/flatpickr/dist/l10n/es.js',
        'node_modules/holderjs/holder.min.js',
        'node_modules/jasny-bootstrap/js/fileinput.js',
        'node_modules/jasny-bootstrap/js/rowlink.js',
        'node_modules/jquery-sticky/jquery.sticky.js',
        'node_modules/jquery-touchswipe/jquery.touchSwipe.min.js',
        'node_modules/selectize/dist/js/standalone/selectize.min.js',
    ], publicPath + '/js/admin.plugins.js')
    .scripts([
        'build/js/_base.js',
        'build/js/components/_datetimepicker.js',
        'build/js/components/_forms.js',
        'build/js/components/_markdown.js',
        'build/js/components/_modals.js',
        'build/js/components/_navbar.js',
        'build/js/components/_selectize.js',
        'build/js/components/_tables.js',
    ], publicPath + '/js/admin.js')
    .sass('build/scss/admin.scss', 'css')
    .options({processCssUrls: false});

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
