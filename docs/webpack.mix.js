let mix = require('laravel-mix');
let build = require('./tasks/build.js');
let path = require('path');
let publicPath = path.normalize('source/assets/build');

mix.disableSuccessNotifications();
mix.setPublicPath(publicPath);
mix.webpackConfig({
    plugins: [
        build.jigsaw,
        build.browserSync(),
        build.watch(['source/**/*.md', 'source/**/*.php', 'source/**/*.scss', '!source/**/_tmp/*']),
    ]
});

mix.copy('../dist/js/admin.js', publicPath + '/js/admin.js')
    .copy('../dist/js/admin.plugins.js', publicPath + '/js/admin.plugins.js')
    .copy('../dist/css/admin.css', publicPath + '/css/admin.css')
    .copy('source/_assets/js/docs.js', publicPath + '/js/docs.js')
    .sass('source/_assets/sass/docs.scss', 'css')
    .options({
        processCssUrls: false,
    }).version();
