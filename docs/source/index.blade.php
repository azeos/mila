@extends('_layouts.default')

@section('content')
    <h1 class="docs-title">Introducción</h1>
    <p class="docs-lead">{{ $page->title }} es un panel de administración basado en <a href="https://getbootstrap.com/" target="_blank">Bootstrap 4</a>.</p>
    <h2 class="docs-section-title" id="instalacion">Instalación</h2>
    <h2 class="docs-section-title" id="plantilla">Plantilla</h2>
<pre><code>&lt;!doctype html&gt;
&lt;html lang=&quot;es&quot;&gt;
    &lt;head&gt;
        &lt;meta charset=&quot;utf-8&quot;&gt;
        &lt;meta http-equiv=&quot;x-ua-compatible&quot; content=&quot;ie=edge&quot;&gt;
        &lt;title&gt;T&iacute;tulo&lt;/title&gt;
        &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1, shrink-to-fit=no&quot;&gt;
        &lt;link href=&quot;https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&quot; rel=&quot;stylesheet&quot;&gt;
        &lt;link href=&quot;https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css&quot; rel=&quot;stylesheet&quot; integrity=&quot;sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS&quot; crossorigin=&quot;anonymous&quot;&gt;
        &lt;script src=&quot;https://use.fontawesome.com/releases/v5.6.3/js/all.js&quot; integrity=&quot;sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1&quot; crossorigin=&quot;anonymous&quot; defer&gt;&lt;/script&gt;
        &lt;link rel=&quot;stylesheet&quot; href=&quot;css/admin.css&quot;&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;header class=&quot;mobile-header navbar navbar-expand navbar-dark bg-dark fixed-top&quot;&gt;
            &lt;!-- Mobile header --&gt;
        &lt;/header&gt;
        &lt;nav class=&quot;main-navbar&quot;&gt;
            &lt;!-- Main navbar --&gt;
        &lt;/nav&gt;
        &lt;div id=&quot;app&quot; class=&quot;main-container&quot;&gt;
            &lt;header class=&quot;main-header navbar navbar-expand navbar-light&quot;&gt;
                &lt;!-- Main header --&gt;
            &lt;/header&gt;
            &lt;div class=&quot;page-wrapper&quot;&gt;
                &lt;!-- Page content --&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;script src=&quot;https://code.jquery.com/jquery-3.3.1.min.js&quot; integrity=&quot;sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
        &lt;script src=&quot;https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js&quot; integrity=&quot;sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
        &lt;script src=&quot;js/admin.plugins.js&quot;&gt;&lt;/script&gt;
        &lt;script src=&quot;js/admin.js&quot;&gt;&lt;/script&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>
    <h2 class="docs-section-title" id="dependencias">Dependencias</h2>
    <p>{{ $page->title }} depende de las siguientes librerías/frameworks; no están incluidas, se recomienda utilizar un CDN:</p>
    <ul>
        <li><a href="https://jquery.com/" target="_blank">jQuery 3</a></li>
        <li><a href="https://getbootstrap.com/" target="_blank">Bootstrap 4</a></li>
        <li><a href="https://fontawesome.com/" target="_blank">Font Awesome 5</a></li>
    </ul>
    <pre><code>&lt;link href=&quot;https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css&quot; rel=&quot;stylesheet&quot; integrity=&quot;sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS&quot; crossorigin=&quot;anonymous&quot;&gt;
&lt;script src=&quot;https://code.jquery.com/jquery-3.3.1.min.js&quot; integrity=&quot;sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js&quot; integrity=&quot;sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://use.fontawesome.com/releases/v5.6.3/js/all.js&quot; integrity=&quot;sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1&quot; crossorigin=&quot;anonymous&quot; defer&gt;&lt;/script&gt;</code></pre>
    <h2 class="docs-section-title" id="plugins">Plugins</h2>
    <p>{{ $page->title }} usa los siguientes plugins. Para la documentación revisar los siguientes links:</p>
    <ul>
        <li><a href="https://datatables.net/" target="_blank">DataTables ^1.10.19</a></li>
        <li><a href="https://www.dropzonejs.com/" target="_blank">DropzoneJS ^5.5.1</a></li>
        <li><a href="https://github.com/ionaru/easy-markdown-editor" target="_blank">EasyMDE ^2.4.2</a></li>
        <li><a href="https://flatpickr.js.org/" target="_blank">flatpickr ^4.5.2</a></li>
        <li><a href="http://holderjs.com/" target="_blank">Holder.js ^2.9.6</a></li>
        <li><a href="http://www.jasny.net/bootstrap/javascript/" target="_blank">Jasny Bootstrap ^3.1.3</a></li>
        <li><a href="http://stickyjs.com/" target="_blank">Sticky ^1.0.4</a></li>
        <li><a href="http://labs.rampinteractive.co.uk/touchSwipe/demos/index.html" target="_blank">TouchSwipe ^1.6.19</a></li>
        <li><a href="https://momentjs.com/" target="_blank">Moment.js ^2.22.2</a></li>
        <li><a href="http://selectize.github.io/selectize.js/" target="_blank">Selectize.js ^0.12.6</a></li>
    </ul>
    <h2 class="docs-section-title" id="fonts">Fonts</h2>
    <p>{{ $page->title }} usa las siguiente tipografías. Si se quieren utilizar otras, editar el archivo <code>_variables.scss</code>:</p>
    <ul>
        <li><a href="https://fonts.google.com/specimen/Montserrat" target="_blank">Montserrat</a></li>
        <li><a href="https://fonts.google.com/specimen/Open+Sans" target="_blank">Open Sans</a></li>
    </ul>
    <pre><code>&lt;link href=&quot;https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&quot; rel=&quot;stylesheet&quot;&gt;</code></pre>
@endsection
