// Test form
$('body').on('submit', '.form-test', function (e) {
    // Previene el envío
    e.preventDefault();

    // Estado del botón de submit
    // Si method == DELETE, selecciona el btn-delete
    var $method = $(this).find('input[name="_method"]');
    if ($method.length && $method.val() == 'DELETE') {
        var $btn = $(this).find('.btn-delete');
    } else {
        var $btn = $(this).find('.btn-state');
    }
    buttonState($btn, 'loading');

    // Resetea el botón de submit
    setTimeout(function () {
        buttonState($btn, 'success');
    }, 2000);
});

// Stateful buttons
$('#btnStateLoading').on('click', function () {
    var $btn = $(this);
    buttonState($btn, 'loading');
    setTimeout(function () {
        buttonState($btn, 'reset');
    }, 2000);
});
$('#btnStateWarning').on('click', function () {
    var $btn = $(this);
    buttonState($btn, 'loading');
    setTimeout(function () {
        buttonState($btn, 'warning');
    }, 2000);
});