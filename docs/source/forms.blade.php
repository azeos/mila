@extends('_layouts.default', ['title' => 'Formularios'])

@section('content')
    <h1 class="docs-title">Formularios</h1>
    <p class="docs-lead">Listado de todas las librerías y código propio utilizados para la creación de formularios.</p>
    <h2 class="docs-section-title" id="stateful-button">Stateful Button</h2>
    <p>
        <code>buttonState($btn, state)</code> es una función basada en el <a href="https://getbootstrap.com/docs/3.3/javascript/#buttons-stateful" target="_blank">stateful button</a> de Bootstrap 3.
        <br>
        Cambia el texto y la clase del botón para reflejar el "estado" del mismo (como un mensaje de loading).
    </p>
    <p>Hay 2 estados que vienen por defecto:</p>
    <ul>
        <li><code>reset</code> contiene el texto original del botón y, si se está usando alguna clase de <a href="https://getbootstrap.com/docs/4.3/components/buttons/#examples" target="_blank">Bootstrap</a>, la clase correspondiente.</li>
        <li><code>loading</code> es un spinner de <a href="https://fontawesome.com/" target="_blank">FA5</a> <i class="fas fa-spinner fa-pulse"></i> <code>&lt;i class=&quot;fas fa-spinner fa-pulse&quot;&gt;&lt;/i&gt;</code>. Al aplicar éste estado el botón se deshabilita.</li>
    </ul>
    <p>
        Los textos de los estados se definen como <code>data-estadox-text</code>, las clases como <code>data-estadox-class</code> y se los aplica llamado a <code>buttonState($btn, 'estadox')</code>.
        <br>
        Si se llama a un estado que no existe, se aplica <code>reset</code>.
    </p>
    <div class="docs-example">
        <button id="btnStateLoading" class="btn btn-primary">GUARDAR</button>
    </div>
    <pre><code>&lt;button id=&quot;btnStateLoading&quot; class=&quot;btn btn-primary&quot;&gt;GUARDAR&lt;/button&gt;
&lt;script&gt;
    $('#btnStateLoading').on('click', function () {
        var $btn = $(this);
        buttonState($btn, 'loading');
        // L&oacute;gica de la aplicaci&oacute;n...
        buttonState($btn, 'reset');
    });
&lt;/script&gt;</code></pre>
    <div class="docs-example">
        <button id="btnStateWarning" class="btn btn-primary" data-warning-class="btn-warning" data-warning-text='Completá todos los campos <i class="fas fa-exclamation-triangle"></i>'>GUARDAR</button>
    </div>
    <pre><code>&lt;button id=&quot;btnStateWarning&quot; class=&quot;btn btn-primary&quot; data-warning-class=&quot;btn-warning&quot; data-warning-text='Complet&aacute; todos los campos &lt;i class=&quot;fas fa-exclamation-triangle&quot;&gt;&lt;/i&gt;'&gt;GUARDAR&lt;/button&gt;
&lt;script&gt;
    $('#btnStateWarning').on('click', function () {
        var $btn = $(this);
        buttonState($btn, 'loading');
        // L&oacute;gica de la aplicaci&oacute;n...
        buttonState($btn, 'warning');
    });
&lt;/script&gt;</code></pre>
    <h2 class="docs-section-title" id="form-ajax">Form AJAX</h2>
    <p>Si se le asigna la clase <code>.form-ajax</code> a un formulario, éste se envía a través de AJAX.</p>
    <ul>
        <li>Si hay un botón con la clase <code>.btn-state</code>, le aplica <code>buttonState($btn, 'loading')</code> mientras se envía el formulario.</li>
        <li>Si el <code>_method</code> es <code>DELELTE</code>, le aplica el loading al botón <code>.btn-delete</code>.</li>
        <li>Si el resultado del envío es un <em>string</em>, se asume que es una URL y se redirecciona a dicha dirección.</li>
        <li>Sino se dispara el <em>evento</em> <code>form.ajax.success</code> y <code>buttonState($btn, 'success')</code>.</li>
        <li>Si falla el envío se muestran los errores en un <code>&lt;div&gt;</code> con la clase <code>.ajax-errors</code>. Si el estado es 422 se dispara <code>buttonState($btn, 'warning')</code> y sino <code>buttonState($btn, 'fail')</code>.</li>
        <li>Si el formulario se encuentra en un <a href="https://getbootstrap.com/docs/4.3/components/modal/" target="_blank">modal</a> hay que agregar la clase <code>.form-modal</code> e incluir dentro del mismo un <code>.ajax-errors</code>. De esa forma se muestran los errores del formulario dentro del modal.</li>
        <li>
            Se espera que la respuesta del error contenga uno de los siguientes elementos:
            <ul>
                <li>Un <em>array</em> llamado <em>errors</em>, es lo que devuelve <a href="https://laravel.com/docs/5.7/validation#quick-displaying-the-validation-errors" target="_blank">Laravel</a> por defecto al fallar una validación.</li>
                <li>Un <em>array</em> llamado <em>flash</em> con <em>keys</em> llamadas <em>message</em>, es lo que se devuelve desde los controladores para los errores propios.</li>
                <li>Un <em>key</em> llamado <em>statusText</em> con el texto del error.</li>
            </ul>
        </li>
    </ul>
    <h2 class="docs-section-title" id="bootbox">Bootbox</h2>
    <p>Documentación oficial: <a href="http://bootboxjs.com/" target="_blank">bootboxjs.com</a>.</p>
    <p>Bootbox es utilizado para mostrar mensajes de alerta al borrar un elemento.</p>
    <ul>
        <li>Aplicar la clase <code>.btn-delete</code> al botón encargado de eliminar un elemento.</li>
        <li>Se usa <code>name="_method" value="DELETE"</code> para hacer un <a href="https://laravel.com/docs/5.7/routing#form-method-spoofing" target="_blank">method spoofing</a>.</li>
        <li>El mensaje a mostrar se define en el atributo <code>data-bbox-msg</code>.</li>
        <li>Al aceptar el mensaje, el formulario en el que se encuentra el botón es enviado automáticamente.</li>
    </ul>
    <div class="docs-example">
        <form action="/" method="POST" class="form-test">
            <button type="button" name="_method" value="DELETE" class="btn btn-outline-danger btn-delete" data-bbox-msg='¿Desea eliminar el elemento "Elemento X" permanentemente?'>ELIMINAR</button>
        </form>
    </div>
    <pre><code class="html">&lt;button type=&quot;button&quot; name=&quot;_method&quot; value=&quot;DELETE&quot; class=&quot;btn btn-outline-danger btn-delete&quot; data-bbox-msg='&iquest;Desea eliminar el elemento "Elemento X" permanentemente?'&gt;ELIMINAR&lt;/button&gt;</code></pre>
    <h2 class="docs-section-title" id="dropzonejs">DropzoneJS</h2>
    <p>Documentación oficial: <a href="https://www.dropzonejs.com/" target="_blank">dropzonejs.com</a>.</p>
    <p>DropzoneJS permite subir varios archivos simultáneamente mediante drag'n'drop.</p>
    <p>Crear un contenedor con la clase <code>.dropzone-upload</code>.</p>
    <ul>
        <li>Si hay un botón con la clase <code>.btn-addFile</code> será <a href="https://www.dropzonejs.com/#config-clickable" target="_blank">clickable</a>.</li>
        <li>Usar la clase <code>.dropzone-template</code> para especificar el <a href="https://www.dropzonejs.com/#config-previewTemplate" target="_blank">previewTemplate</a> y el <a href="https://www.dropzonejs.com/#config-previewsContainer" target="_blank">previewsContainer</a>. A ese mismo <code>&lt;div&gt;</code> sumarle la clase <code>.thumbnail-upload</code> para aplicar los estilos correspondientes.</li>
        <li>El <code>name</code> de los campos extra que se quieran enviar tiene que ser <code>fotos[fotoId][nombreDelCampo]</code>. <code>fotoId</code> es reemplazado automáticamente por el ID del archivo.</li>
        <li>Para especificar el <code>value</code> de los <code>&lt;input&gt;</code>s precargados, utilizar <code>data-value=""</code>. Pasar como valor el nombre del campo del objeto <em>fotos</em> <code>data-value="estreno"</code>. Si se quiere mostrar el valor de un campo que está dentro de <em>translations</em> usar: <code>data-value='[{"array": "translations", "value": "titulo"}]'</code>. Pasar un <em>objeto</em> como valor solo sirve para las traducciones porque busca obligatoriamente la <em>key</em> <code>locale</code>.</li>
        <li>Si se quiere hacer que un campo sea <code>readonly</code> de manera condicional, usar <code>data-readonly=""</code>. Pasar como valor el nombre de algún campo del objeto <em>fotos</em>, si se evalúa como <em>true</em> se le aplica el atributo <code>readonly</code>.</li>
        <li><code>.btn-hide</code> se utiliza para mostrar u ocultar la imagen. Al clickearlo busca dentro suyo un campo con la clase <code>.foto-hide</code> al que le asigna <code>value="1"</code> si la imagen está oculta y <code>valu="0"</code> si no lo está.</li>
    </ul>
    <p>Las opciones se pasan mediante el atributo data. Hay que agregar el nombre de la opción seguido de <code>data-</code>, por ejemplo <code>data-auto-process-queue="false"</code>.</p>
    <div class="card rounded-0 mb-3">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-docs">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Default</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-url" target="_blank">url</a></td>
                        <td>string</td>
                        <td></td>
                        <td>URL a la que se envían los archivos. Es obligatorio, excepto que el contenedor sea un <code>&lt;form&gt;</code> con un <code>action=""</code>.</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-autoProcessQueue" target="_blank">autoProcessQueue</a></td>
                        <td>boolean</td>
                        <td>true</td>
                        <td>Indica si los archivos se envían o no automáticamente.</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-maxFilesize" target="_blank">maxFilesize</a></td>
                        <td>integer</td>
                        <td>2</td>
                        <td>Peso máximo de cada archivo expresado en MB.</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-maxFiles" target="_blank">maxFiles</a></td>
                        <td>integer</td>
                        <td>null</td>
                        <td>Cantidad máxima de archivos que se pueden subir.</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-paramName" target="_blank">paramName</a></td>
                        <td>string</td>
                        <td>'image'</td>
                        <td>El <code>name</code> del archivo enviado.</td>
                    </tr>
                    <tr>
                        <td><a href="https://www.dropzonejs.com/#config-acceptedFiles" target="_blank">acceptedFiles</a></td>
                        <td>string</td>
                        <td>'image/*'</td>
                        <td>Valor del atributo <code>accept</code>.</td>
                    </tr>
                    <tr>
                        <td>idField</td>
                        <td>string</td>
                        <td>'id'</td>
                        <td>Nombre del campo que contiene el ID.</td>
                    </tr>
                    <tr>
                        <td>urlField</td>
                        <td>string</td>
                        <td>'imagenUrl'</td>
                        <td>Nombre del campo que contiene la URL de la imagen.</td>
                    </tr>
                    <tr>
                        <td>hiddenField</td>
                        <td>string</td>
                        <td>'deleted_at'</td>
                        <td>Nombre del campo que indica si la imagen está o no oculta.</td>
                    </tr>
                    <tr>
                        <td>fotos</td>
                        <td>object</td>
                        <td>null</td>
                        <td>
                            <p>Objeto que contiene los archivos que tienen que ser precargados.</p>
                            <p>Se esperan las siguientes <em>keys</em> (que se definen con las opciones que están más arriba):</p>
                            <ul>
                                <li>id: ID de la base de datos.</li>
                                <li>imagenUrl: URL de la imagen.</li>
                                <li>deleted_at: Determina si la imagen está oculta.</li>
                                <li>translations: Es opcional, contiene las traducciones del elemento. La <em>key</em> <code>locale</code> es obligatoria y debe coincidir con el atributo <code>lang</code> del <code>&lt;input&gt;</code>. Se puede usar cualquier otro nombre como <em>key</em>, cuando se use <code>data-value=""</code> usar el nombre que corresponda en <code>"array"</code>.</li>
                            </ul>
                            <p>Ejemplo:</p>
                            <pre><code>[
    {
        "id": 1,
        "imagenUrl": "{{ $page->baseUrl }}/assets/images/lost-in-translation.jpg",
        "deleted_at": null,
        "estreno": "2003",
        "translations": [
            {
                "titulo": "Perdidos en Tokio",
                "locale": "es"
            },
            {
                "titulo": "Lost in Translation",
                "locale": "en"
            }
        ]
    },
    {
        "id": 2,
        "imagenUrl": "{{ $page->baseUrl }}/assets/images/pulp-fiction.jpg",
        "deleted_at": "1994-10-14 00:00:00",
        "estreno": "1994",
        "translations": [
            {
                "titulo": "Tiempos violentos",
                "locale": "es"
            },
            {
                "titulo": "Pulp Fiction",
                "locale": "en"
            }
        ]
    }
]</code></pre>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <p>Ejemplo sin imágenes precargadas:</p>
    <div class="docs-example">
        <div class="dropzone-upload" data-auto-process-queue="false" data-url="/">
            <div class="row">
                <div class="col">
                    <div class="btn-toolbar mb-3">
                        <div class="btn-group btn-group-sm ml-auto">
                            <button type="button" class="btn btn-secondary btn-addFile"><i class="fas fa-plus"></i> Fotos</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sortable thumbnail-upload dropzone-template">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                    <div class="card sortable-handle">
                        <img class="card-img-top" data-dz-thumbnail>
                        <div class="card-body">
                            <div class="progress d-none">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                            <div class="btn-group btn-group-sm float-right">
                                <button type="button" class="btn btn-secondary btn-hide" data-toggle="tooltip" data-container="body" title="Ocultar">
                                    <i class="btn-hide-icon fas fa-eye-slash"></i>
                                    <input type="hidden" name="fotos[fotoId][hide]" class="foto-hide">
                                </button>
                                <button type="button" class="btn btn-danger" data-toggle="tooltip" data-container="body" title="Eliminar" data-dz-remove><i class="fas fa-trash-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;div class=&quot;dropzone-upload&quot; data-url=&quot;/&quot;&gt;
    &lt;div class=&quot;row&quot;&gt;
        &lt;div class=&quot;col&quot;&gt;
            &lt;div class=&quot;btn-toolbar mb-3&quot;&gt;
                &lt;div class=&quot;btn-group btn-group-sm ml-auto&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-secondary btn-addFile&quot;&gt;&lt;i class=&quot;fas fa-plus&quot;&gt;&lt;/i&gt; Fotos&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;row sortable thumbnail-upload dropzone-template&quot;&gt;
        &lt;div class=&quot;col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3&quot;&gt;
            &lt;div class=&quot;card sortable-handle&quot;&gt;
                &lt;img class=&quot;card-img-top&quot; data-dz-thumbnail&gt;
                &lt;div class=&quot;card-body&quot;&gt;
                    &lt;div class=&quot;progress d-none&quot;&gt;
                        &lt;div class=&quot;progress-bar progress-bar-striped progress-bar-animated bg-success&quot; style=&quot;width:0%;&quot; data-dz-uploadprogress&gt;&lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;btn-group btn-group-sm float-right&quot;&gt;
                        &lt;button type=&quot;button&quot; class=&quot;btn btn-secondary btn-hide&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; title=&quot;Ocultar&quot;&gt;
                            &lt;i class=&quot;btn-hide-icon fas fa-eye-slash&quot;&gt;&lt;/i&gt;
                            &lt;input type=&quot;hidden&quot; name=&quot;fotos[fotoId][hide]&quot; class=&quot;foto-hide&quot;&gt;
                        &lt;/button&gt;
                        &lt;button type=&quot;button&quot; class=&quot;btn btn-danger&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; title=&quot;Eliminar&quot; data-dz-remove&gt;&lt;i class=&quot;fas fa-trash-alt&quot;&gt;&lt;/i&gt;&lt;/button&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <p>El texto "Arrastre los archivos aquí." se puede traducir modificando la variable <code>$admin-dropzone-text</code> usando el código del idioma. Se mostrará el idioma definido por el atributo <code>lang</code>.</p>
    <p>Los textos del <code>.btn-hide</code> se pueden cambiar mediante <code>data-show-text=""</code> y <code>data-hide-text=""</code>.</p>
    <pre><code class="scss">$admin-dropzone-text: (
    es: 'Arrastre los archivos aquí.',
    en: 'Drop files here.'
)</code></pre>
    <div class="docs-example">
        <div class="dropzone-upload" data-auto-process-queue="false" data-url="/" lang="en">
            <div class="row sortable thumbnail-upload dropzone-template">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                    <div class="card sortable-handle">
                        <img class="card-img-top" data-dz-thumbnail>
                        <div class="card-body">
                            <div class="progress d-none">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                            <div class="btn-group btn-group-sm float-right">
                                <button type="button" class="btn btn-secondary btn-hide" data-toggle="tooltip" data-container="body" data-show-text="Show" data-hide-text="Hide" title="Hide">
                                    <i class="btn-hide-icon fas fa-eye-slash"></i>
                                    <input type="hidden" name="fotos[fotoId][hide]" class="foto-hide">
                                </button>
                                <button type="button" class="btn btn-danger" data-toggle="tooltip" data-container="body" title="Delete" data-dz-remove><i class="fas fa-trash-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;div class=&quot;dropzone-upload&quot; data-url=&quot;/&quot; lang=&quot;en&quot;&gt;
    ...
    &lt;div class=&quot;btn-group btn-group-sm float-right&quot;&gt;
        &lt;button type=&quot;button&quot; class=&quot;btn btn-secondary btn-hide&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; data-show-text=&quot;Show&quot; data-hide-text=&quot;Hide&quot; title=&quot;Hide&quot;&gt;
            &lt;i class=&quot;btn-hide-icon fas fa-eye-slash&quot;&gt;&lt;/i&gt;
            &lt;input type=&quot;hidden&quot; name=&quot;fotos[fotoId][hide]&quot; class=&quot;foto-hide&quot;&gt;
        &lt;/button&gt;
        &lt;button type=&quot;button&quot; class=&quot;btn btn-danger&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; title=&quot;Delete&quot; data-dz-remove&gt;&lt;i class=&quot;fas fa-trash-alt&quot;&gt;&lt;/i&gt;&lt;/button&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <p>Ejemplo con imágenes precargadas y <em>translations</em>:</p>
    <div class="docs-example">
        <div class="dropzone-upload" data-auto-process-queue="false" data-url="/" data-fotos='[{"id":1,"imagenUrl":"{{ $page->baseUrl }}/assets/images/lost-in-translation.jpg","deleted_at":null,"estreno":"2003","translations":[{"titulo":"Perdidos en Tokio","locale":"es"},{"titulo":"Lost in Translation","locale":"en"}]},{"id":2,"imagenUrl":"{{ $page->baseUrl }}/assets/images/pulp-fiction.jpg","deleted_at":"1994-10-14 00:00:00","estreno":"1994","translations":[{"titulo":"Tiempos violentos","locale":"es"},{"titulo":"Pulp Fiction","locale":"en"}]}]'>
            <div class="row">
                <div class="col">
                    <div class="btn-toolbar mb-3">
                        <div class="btn-group btn-group-sm ml-auto">
                            <button type="button" class="btn btn-secondary btn-addFile"><i class="fas fa-plus"></i> Fotos</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sortable thumbnail-upload dropzone-template">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                    <div class="card sortable-handle">
                        <img class="card-img-top" data-dz-thumbnail>
                        <div class="card-body">
                            <div class="progress d-none">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                            <ul class="nav nav-tabs mb-3">
                                @foreach ($page->locales as $code => $lang)
                                    <li class="nav-item"><a href="#fotoId{{ $code }}" class="nav-link{{ $loop->first ? ' active' : '' }}" data-toggle="tab">{{ $lang }}</a></li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                @foreach ($page->locales as $code => $lang)
                                    <div class="tab-pane{{ $loop->first ? ' active' : '' }}" id="fotoId{{ $code }}">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input name="fotos[fotoId][titulo][{{ $code }}]" class="form-control" placeholder="Título" data-value='[{"array": "translations", "value": "titulo"}]' lang="{{ $code }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <input name="fotos[fotoId][estreno]" class="form-control" placeholder="Año" data-value="estreno">
                            </div>
                            <div class="btn-group btn-group-sm float-right">
                                <button type="button" class="btn btn-secondary btn-hide" data-toggle="tooltip" data-container="body" title="Ocultar">
                                    <i class="btn-hide-icon fas fa-eye-slash"></i>
                                    <input type="hidden" name="fotos[fotoId][hide]" class="foto-hide">
                                </button>
                                <button type="button" class="btn btn-danger" data-toggle="tooltip" data-container="body" title="Eliminar" data-dz-remove><i class="fas fa-trash-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;div class=&quot;dropzone-upload&quot; data-url=&quot;/&quot; data-fotos='[{&quot;id&quot;:1,&quot;imagenUrl&quot;:&quot;{{ $page->baseUrl }}/assets/images/lost-in-translation.jpg&quot;,&quot;deleted_at&quot;:null,&quot;estreno&quot;:&quot;2003&quot;,&quot;translations&quot;:[{&quot;titulo&quot;:&quot;Perdidos en Tokio&quot;,&quot;locale&quot;:&quot;es&quot;},{&quot;titulo&quot;:&quot;Lost in Translation&quot;,&quot;locale&quot;:&quot;en&quot;}]},{&quot;id&quot;:2,&quot;imagenUrl&quot;:&quot;{{ $page->baseUrl }}/assets/images/pulp-fiction.jpg&quot;,&quot;deleted_at&quot;:&quot;1994-10-14 00:00:00&quot;,&quot;estreno&quot;:&quot;1994&quot;,&quot;translations&quot;:[{&quot;titulo&quot;:&quot;Tiempos violentos&quot;,&quot;locale&quot;:&quot;es&quot;},{&quot;titulo&quot;:&quot;Pulp Fiction&quot;,&quot;locale&quot;:&quot;en&quot;}]}]'&gt;
    &lt;div class=&quot;row&quot;&gt;
        &lt;div class=&quot;col&quot;&gt;
            &lt;div class=&quot;btn-toolbar mb-3&quot;&gt;
                &lt;div class=&quot;btn-group btn-group-sm ml-auto&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-secondary btn-addFile&quot;&gt;&lt;i class=&quot;fas fa-plus&quot;&gt;&lt;/i&gt; Fotos&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;row sortable thumbnail-upload dropzone-template&quot;&gt;
        &lt;div class=&quot;col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3&quot;&gt;
            &lt;div class=&quot;card sortable-handle&quot;&gt;
                &lt;img class=&quot;card-img-top&quot; data-dz-thumbnail&gt;
                &lt;div class=&quot;card-body&quot;&gt;
                    &lt;div class=&quot;progress d-none&quot;&gt;
                        &lt;div class=&quot;progress-bar progress-bar-striped progress-bar-animated bg-success&quot; style=&quot;width:0%;&quot; data-dz-uploadprogress&gt;&lt;/div&gt;
                    &lt;/div&gt;
                    &lt;ul class=&quot;nav nav-tabs mb-3&quot;&gt;
@foreach ($page->locales as $code => $lang)
                        &lt;li class=&quot;nav-item&quot;&gt;&lt;a href=&quot;#fotoId{{ $code }}&quot; class=&quot;nav-link{{ $loop->first ? ' active' : '' }}&quot; data-toggle=&quot;tab&quot;&gt;{{ $lang }}&lt;/a&gt;&lt;/li&gt;
@endforeach
                    &lt;/ul&gt;
                    &lt;div class=&quot;tab-content&quot;&gt;
@foreach ($page->locales as $code => $lang)
                        &lt;div class=&quot;tab-pane{{ $loop->first ? ' active' : '' }}&quot; id=&quot;fotoId{{ $code }}&quot;&gt;
                            &lt;div class=&quot;row&quot;&gt;
                                &lt;div class=&quot;col-12&quot;&gt;
                                    &lt;div class=&quot;form-group&quot;&gt;
                                        &lt;input name=&quot;fotos[fotoId][titulo][{{ $code }}]&quot; class=&quot;form-control&quot; placeholder=&quot;T&iacute;tulo&quot; data-value='[{&quot;array&quot;: &quot;translations&quot;, &quot;value&quot;: &quot;titulo&quot;}]' lang=&quot;{{ $code }}&quot;&gt;
                                    &lt;/div&gt;
                                &lt;/div&gt;
                            &lt;/div&gt;
                        &lt;/div&gt;
@endforeach
                    &lt;/div&gt;
                    &lt;div class=&quot;form-group&quot;&gt;
                        &lt;input name=&quot;fotos[fotoId][estreno]&quot; class=&quot;form-control&quot; placeholder=&quot;A&ntilde;o&quot; data-value=&quot;estreno&quot;&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;btn-group btn-group-sm float-right&quot;&gt;
                        &lt;button type=&quot;button&quot; class=&quot;btn btn-secondary btn-hide&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; title=&quot;Ocultar&quot;&gt;
                            &lt;i class=&quot;btn-hide-icon fas fa-eye-slash&quot;&gt;&lt;/i&gt;
                            &lt;input type=&quot;hidden&quot; name=&quot;fotos[fotoId][hide]&quot; class=&quot;foto-hide&quot;&gt;
                        &lt;/button&gt;
                        &lt;button type=&quot;button&quot; class=&quot;btn btn-danger&quot; data-toggle=&quot;tooltip&quot; data-container=&quot;body&quot; title=&quot;Eliminar&quot; data-dz-remove&gt;&lt;i class=&quot;fas fa-trash-alt&quot;&gt;&lt;/i&gt;&lt;/button&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <h2 class="docs-section-title" id="easymde">EasyMDE</h2>
    <p>Documentación oficial: <a href="https://github.com/ionaru/easy-markdown-editor" target="_blank">ionaru/easy-markdown-editor</a>.</p>
    <p>EasyMDE es un <a href="https://simplemde.com/markdown-guide" target="_blank">Markdown</a> editor.</p>
    <p>Simplemente hay que agregarle la clase <code>.markdown</code> a un textarea. Soporta el atributo <code>required</code>.</p>
    <div class="docs-example">
        <form>
            <textarea class="form-control markdown" required>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequatur corporis cum esse eveniet exercitationem fuga itaque maiores quisquam suscipit, vero voluptatum. Beatae est explicabo fugiat ipsum libero nisi quas?</textarea>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>
    <pre><code class="html">&lt;textarea class=&quot;form-control markdown&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad animi, at corporis dolor ducimus illum ipsam laborum magnam omnis optio? Ipsam laboriosam molestiae qui reiciendis similique, sunt! Optio, quia, veritatis?&lt;/textarea&gt;</code></pre>
    <p>Si el textarea se inicializa en un <a href="https://getbootstrap.com/docs/4.3/components/navs/#javascript-behavior" target="_blank">tab</a> oculto, agregarle la clase <code>.has-markdown</code> al tab.</p>
    <div class="docs-example">
        <div class="card rounded-0">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a href="#general" class="nav-link active" data-toggle="tab">General</a>
                    </li>
                    <li class="nav-item">
                        <a href="#texto" class="nav-link has-markdown" data-toggle="tab">Texto</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="general">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, asperiores, autem cupiditate deleniti eaque eligendi enim excepturi harum in magnam nobis non pariatur quasi quibusdam sit sunt veritatis voluptate voluptatem?</p>
                    </div>
                    <div class="tab-pane" id="texto">
                        <textarea class="form-control markdown">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam dicta eos est eum fugiat magni maxime, nam nemo, neque obcaecati, omnis quisquam quo rem sit tenetur totam veniam? Asperiores, laboriosam?</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code>&lt;ul class=&quot;nav nav-tabs&quot;&gt;
    &lt;li class=&quot;nav-item&quot;&gt;
        &lt;a href=&quot;#general&quot; class=&quot;nav-link active&quot; data-toggle=&quot;tab&quot;&gt;General&lt;/a&gt;
    &lt;/li&gt;
    &lt;li class=&quot;nav-item&quot;&gt;
        &lt;a href=&quot;#texto&quot; class=&quot;nav-link has-markdown&quot; data-toggle=&quot;tab&quot;&gt;Texto&lt;/a&gt;
    &lt;/li&gt;
&lt;/ul&gt;
&lt;div class=&quot;tab-content&quot;&gt;
    &lt;div class=&quot;tab-pane fade show active&quot; id=&quot;general&quot;&gt;
        &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, asperiores, autem cupiditate deleniti eaque eligendi enim excepturi harum in magnam nobis non pariatur quasi quibusdam sit sunt veritatis voluptate voluptatem?&lt;/p&gt;
    &lt;/div&gt;
    &lt;div class=&quot;tab-pane&quot; id=&quot;texto&quot;&gt;
        &lt;textarea class=&quot;form-control markdown&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam dicta eos est eum fugiat magni maxime, nam nemo, neque obcaecati, omnis quisquam quo rem sit tenetur totam veniam? Asperiores, laboriosam?&lt;/textarea&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <h2 class="docs-section-title" id="flatpickr">flatpickr</h2>
    <p>Documentación oficial: <a href="https://flatpickr.js.org/" target="_blank">flatpickr.js.org</a>.</p>
    <p>flatpicker es un datetime picker que no depende de ninguna librería externa. Hay una versión para Vue.js, por eso se lo utiliza en vez de otros datepickers.</p>
    <p>Hay que agregarle la clase <code>.datepicker</code> o <code>.datetimepicker</code> a un <code>&lt;input type=&quot;text&quot;&gt;</code>. El formato por defecto es <code>d/m/Y</code>.</p>
    <div class="docs-example">
        <div class="form-group">
            <label for="date">Fecha</label>
            <input type="text" class="form-control datepicker" id="date">
        </div>
        <div class="form-group">
            <label for="datetime">Fecha y hora</label>
            <input type="text" class="form-control datetimepicker" id="datetime">
        </div>
    </div>
    <pre><code class="html">&lt;input type=&quot;text&quot; class=&quot;form-control datepicker&quot;&gt;
&lt;input type=&quot;text&quot; class=&quot;form-control datetimepicker&quot;&gt;</code></pre>
    <p>Se pueden linkear 2 <code>.datepicker</code>s para que uno limite la fecha del otro. Para eso hay que agregar al <code>.datepicker</code> que determina la fecha mínima un <code>data-linked=""</code> con el <code>ID</code> del <code>.datepicker</code> que determina la fecha máxima:</p>
    <div class="docs-example">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="startDate">Fecha de inicio</label>
                    <input type="text" class="form-control datetimepicker" id="startDate" data-linked="endDate">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="endDate">Fecha de fin</label>
                    <input type="text" class="form-control datetimepicker" id="endDate">
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;input type=&quot;text&quot; class=&quot;form-control datetimepicker&quot; data-linked=&quot;endDate&quot;&gt;
&lt;input type=&quot;text&quot; class=&quot;form-control datetimepicker&quot; id=&quot;endDate&quot;&gt;</code></pre>
    <p>Si hay un <code>.datepicker</code> dentro de un <a href="https://getbootstrap.com/docs/4.3/components/modal/" target="_blank">modal</a>, hay que agregarle le clase <code>.has-datepicker</code> al <code>.modal</code>.</p>
    <div class="docs-example">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#datepickerModal">Modal</button>
        <div class="modal fade has-datepicker" id="datepickerModal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" class="form-control datetimepicker">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code>&lt;button type=&quot;button&quot; class=&quot;btn btn-primary&quot; data-toggle=&quot;modal&quot; data-target=&quot;#datepickerModal&quot;&gt;Modal&lt;/button&gt;
&lt;div class=&quot;modal fade has-datepicker&quot; id=&quot;datepickerModal&quot; tabindex=&quot;-1&quot;&gt;
    &lt;div class=&quot;modal-dialog&quot;&gt;
        &lt;div class=&quot;modal-content&quot;&gt;
            &lt;div class=&quot;modal-header&quot;&gt;
                &lt;h5 class=&quot;modal-title&quot;&gt;Modal&lt;/h5&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;
                    &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                &lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body&quot;&gt;
                &lt;input type=&quot;text&quot; class=&quot;form-control datetimepicker&quot;&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <h2 class="docs-section-title" id="holderjs">Holder.js</h2>
    <p>Documentación oficial: <a href="http://holderjs.com/" target="_blank">holderjs.com</a>.</p>
    <p>Holder.js renderiza imágenes (para usar de placeholders) directamente en el navegador.</p>
    <div class="docs-example">
        <img data-src="holder.js/100px250" class="img-fluid" alt="100%x250">
    </div>
    <pre><code class="html">&lt;img data-src=&quot;holder.js/100px250&quot; class=&quot;img-thumbnail&quot; alt=&quot;100%x250&quot;&gt;</code></pre>
    <h2 class="docs-section-title" id="jasny-bootstrap">Jasny Bootstrap</h2>
    <p>Documentación oficial: <a href="http://www.jasny.net/bootstrap/javascript/" target="_blank">jasny.net/bootstrap</a>.</p>
    <p>Jasny Bootstrap es una librería que extiende algunas funcionalidades de Bootstrap. Se utiliza <a href="http://www.jasny.net/bootstrap/javascript/#fileinput" target="_blank">File input</a> y <a href="http://www.jasny.net/bootstrap/javascript/#rowlink" target="_blank">Row link</a>.</p>
    <p><a href="http://www.jasny.net/bootstrap/javascript/#fileinput" target="_blank">File input</a> permite ver un preview al subir una imagen.</p>
    <div class="docs-example">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" data-trigger="fileinput">
                        <img data-src="holder.js/200x200?auto=yes&amp;textmode=literal" class="img-thumbnail w-100" alt="Imagen">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput"></div>
                    <div>
                        <span class="btn btn-light btn-file">
                            <span class="fileinput-new">Buscar</span>
                            <span class="fileinput-exists">Cambiar</span>
                            <input type="file" accept="image/*">
                        </span>
                        <a href="#" class="btn btn-light fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;div class=&quot;fileinput fileinput-new&quot; data-provides=&quot;fileinput&quot;&gt;
    &lt;div class=&quot;fileinput-new thumbnail&quot; data-trigger=&quot;fileinput&quot;&gt;
        &lt;img data-src=&quot;holder.js/200x200?auto=yes&amp;amp;textmode=literal&quot; class=&quot;img-thumbnail w-100&quot; alt=&quot;Imagen&quot;&gt;
    &lt;/div&gt;
    &lt;div class=&quot;fileinput-preview fileinput-exists thumbnail&quot; data-trigger=&quot;fileinput&quot;&gt;&lt;/div&gt;
    &lt;div&gt;
        &lt;span class=&quot;btn btn-light btn-file&quot;&gt;
            &lt;span class=&quot;fileinput-new&quot;&gt;Buscar&lt;/span&gt;
            &lt;span class=&quot;fileinput-exists&quot;&gt;Cambiar&lt;/span&gt;
            &lt;input type=&quot;file&quot; accept=&quot;image/*&quot;&gt;
        &lt;/span&gt;
        &lt;a href=&quot;#&quot; class=&quot;btn btn-light fileinput-exists&quot; data-dismiss=&quot;fileinput&quot;&gt;Eliminar&lt;/a&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <p><a href="http://www.jasny.net/bootstrap/javascript/#rowlink" target="_blank">Row link</a> transforma la fila de una tabla en un link clickeable.</p>
    <div class="docs-example">
        <i class="far fa-eye-slash" data-fa-symbol="view"></i>
        <div class="card rounded-0">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="rowlink" data-link="row">
                        <tr>
                            <td><a href="{{ $page->baseUrl }}/forms/#bootbox">Bootbox</a></td>
                            <td>Toda esta fila es clickeable y lleva a la documentación de <a href="{{ $page->baseUrl }}/forms/#bootbox">Bootbox</a></td>
                            <td class="table-btns rowlink-skip">
                                <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" title="Fila excluida del rowlink">
                                    <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ $page->baseUrl }}/forms/#holderjs">Holder.js</a></td>
                            <td>Toda esta fila es clickeable y lleva a la documentación de Holder.js</td>
                            <td class="table-btns rowlink-skip">
                                <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" title="Fila excluida del rowlink">
                                    <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <pre><code>&lt;table class=&quot;table table-hover&quot;&gt;
    &lt;thead&gt;
        &lt;tr&gt;
            &lt;th&gt;Nombre&lt;/th&gt;
            &lt;th&gt;Descripci&oacute;n&lt;/th&gt;
            &lt;th&gt;&lt;/th&gt;
        &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody class=&quot;rowlink&quot; data-link=&quot;row&quot;&gt;
        &lt;tr&gt;
            &lt;td&gt;&lt;a href=&quot;{{ $page->baseUrl }}/forms/#bootbox&quot;&gt;Bootbox&lt;/a&gt;&lt;/td&gt;
            &lt;td&gt;Toda esta fila es clickeable y lleva a la documentaci&oacute;n de Bootbox&lt;/td&gt;
            &lt;td class=&quot;table-btns rowlink-skip&quot;&gt;
                &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Fila excluida del rowlink&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/button&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;&lt;a href=&quot;{{ $page->baseUrl }}/forms/#holderjs&quot;&gt;Holder.js&lt;/a&gt;&lt;/td&gt;
            &lt;td&gt;Toda esta fila es clickeable y lleva a la documentaci&oacute;n de Holder.js&lt;/td&gt;
            &lt;td class=&quot;table-btns rowlink-skip&quot;&gt;
                &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Fila excluida del rowlink&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/button&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;</code></pre>
    <h2 class="docs-section-title" id="jquery-ui">jQuery UI</h2>
    <p>Documentación oficial: <a href="http://jqueryui.com/sortable/" target="_blank">jqueryui.com</a>.</p>
    <p>jQuery UI se usa para poder reordenar <code>.table</code>s y <code>.card</code>s.</p>
    <ul>
        <li>Para las tablas hay que agregar la clase <code>.sortable</code> a <code>&lt;table&gt;</code>.</li>
        <li>Para las cartas hay que agregar la clase <code>.sortable</code> al <code>.row</code> que las contiene.</li>
        <li>Se usa la clase <code>.sortable-handle</code> para especificar desde que elementos se puede arrastrar.</li>
        <li>Si los elementos ordenables están dentro de un formulario con la clase <code>.sortable-form</code> y a <code>.sortable</code> se le suma <code>.sortable-ajax</code>, al arrastrar un elemento el formulario se envía automáticamente.</li>
    </ul>
    <div class="docs-example">
        <div class="card rounded-0">
            <h5 class="card-header">Arrastrar para ordenar.</h5>
            <div class="table-responsive">
                <table class="table table-hover sortable">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="sortable-handle">John</td>
                            <td class="sortable-handle">Doe</td>
                            <td class="table-btns">
                                <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" title="Celda no arrastrable">
                                    <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td class="sortable-handle">Jane</td>
                            <td class="sortable-handle">Doe</td>
                            <td class="table-btns">
                                <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" title="Celda no arrastrable">
                                    <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <ul class="nav nav-tabs nav-tabs-code">
        <li class="nav-item">
            <a href="#jquery-ui_table_regular" class="nav-link active" data-toggle="tab">Regular</a>
        </li>
        <li class="nav-item">
            <a href="#jquery-ui_table_ajax" class="nav-link" data-toggle="tab">AJAX</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="jquery-ui_table_regular" class="tab-pane show active">
<pre><code>&lt;table class=&quot;table table-hover sortable&quot;&gt;
    &lt;thead&gt;
        &lt;tr&gt;
            &lt;th&gt;Nombre&lt;/th&gt;
            &lt;th&gt;Apellido&lt;/th&gt;
            &lt;th&gt;&lt;/th&gt;
        &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td class=&quot;sortable-handle&quot;&gt;John&lt;/td&gt;
            &lt;td class=&quot;sortable-handle&quot;&gt;Doe&lt;/td&gt;
            &lt;td class=&quot;table-btns&quot;&gt;
                &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Celda no arrastrable&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/button&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td class=&quot;sortable-handle&quot;&gt;Jane&lt;/td&gt;
            &lt;td class=&quot;sortable-handle&quot;&gt;Doe&lt;/td&gt;
            &lt;td class=&quot;table-btns&quot;&gt;
                &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Celda no arrastrable&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/button&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;</code></pre>
        </div>
        <div id="jquery-ui_table_ajax" class="tab-pane">
            <pre><code class="html">&lt;form class=&quot;sortable-form&quot;&gt;
    &lt;table class=&quot;table table-hover sortable sortable-ajax&quot;&gt;
        &lt;thead&gt;
            &lt;tr&gt;
                &lt;th&gt;Nombre&lt;/th&gt;
                &lt;th&gt;Apellido&lt;/th&gt;
                &lt;th&gt;&lt;/th&gt;
            &lt;/tr&gt;
        &lt;/thead&gt;
        &lt;tbody&gt;
            &lt;tr&gt;
                &lt;td class=&quot;sortable-handle&quot;&gt;John&lt;/td&gt;
                &lt;td class=&quot;sortable-handle&quot;&gt;Doe&lt;/td&gt;
                &lt;td class=&quot;table-btns&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Celda no arrastrable&quot;&gt;
                        &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                    &lt;/button&gt;
                &lt;/td&gt;
            &lt;/tr&gt;
            &lt;tr&gt;
                &lt;td class=&quot;sortable-handle&quot;&gt;Jane&lt;/td&gt;
                &lt;td class=&quot;sortable-handle&quot;&gt;Doe&lt;/td&gt;
                &lt;td class=&quot;table-btns&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Celda no arrastrable&quot;&gt;
                        &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                    &lt;/button&gt;
                &lt;/td&gt;
            &lt;/tr&gt;
        &lt;/tbody&gt;
    &lt;/table&gt;
&lt;/form&gt;</code></pre>
        </div>
    </div>
    <div class="docs-example">
        <div class="row sortable">
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=sky" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 1.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=vine" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 2.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=lava" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 3.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=gray" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 4.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=industrial" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 5.</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card rounded-0 sortable-handle">
                    <img src="holder.js/300x300?auto=yes&amp;textmode=literal&amp;theme=social" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Texto de la foto 6.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <pre><code class="html">&lt;div class=&quot;row sortable&quot;&gt;
    &lt;div class=&quot;col-6 col-md-3&quot;&gt;
        &lt;div class=&quot;card rounded-0 sortable-handle&quot;&gt;
            &lt;img src=&quot;holder.js/300x300?auto=yes&amp;amp;textmode=literal&amp;amp;theme=sky&quot; class=&quot;card-img-top&quot;&gt;
            &lt;div class=&quot;card-body&quot;&gt;
                &lt;p class=&quot;card-text&quot;&gt;Texto de la foto 1.&lt;/p&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;col-6 col-md-3&quot;&gt;
        &lt;div class=&quot;card rounded-0 sortable-handle&quot;&gt;
            &lt;img src=&quot;holder.js/300x300?auto=yes&amp;amp;textmode=literal&amp;amp;theme=vine&quot; class=&quot;card-img-top&quot;&gt;
            &lt;div class=&quot;card-body&quot;&gt;
                &lt;p class=&quot;card-text&quot;&gt;Texto de la foto 2.&lt;/p&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;</code></pre>
    <h2 class="docs-section-title" id="selectizejs">Selectize.js</h2>
    <p>Documentación oficial: <a href="http://selectize.github.io/selectize.js/" target="_blank">selectize.github.io/selectize.js</a>.</p>
    <p>Selectize.js crea <code>&lt;select&gt;</code>s con funcionalidades avanzadas.</p>
    <p>Para un select simple hay que usar la clase <code>.selectize</code>.</p>
    <div class="docs-example">
        <select class="selectize">
            <option value="1">Capital Federal</option>
            <option value="2">Buenos Aires</option>
            <option value="3">Córdoba</option>
        </select>
    </div>
    <pre><code>&lt;select class=&quot;selectize&quot;&gt;
    &lt;option value=&quot;1&quot;&gt;Capital Federal&lt;/option&gt;
    &lt;option value=&quot;2&quot;&gt;Buenos Aires&lt;/option&gt;
    &lt;option value=&quot;3&quot;&gt;C&oacute;rdoba&lt;/option&gt;
&lt;/select&gt;</code></pre>
    <p>Para un select múltiple se usa la misma clase, simplemente se marca como <code>multiple</code> el select.</p>
    <div class="docs-example">
        <select class="selectize" multiple>
            <option value="1">Capital Federal</option>
            <option value="2">Buenos Aires</option>
            <option value="3">Córdoba</option>
        </select>
    </div>
    <pre><code>&lt;select class=&quot;selectize&quot; multiple&gt;
    &lt;option value=&quot;1&quot;&gt;Capital Federal&lt;/option&gt;
    &lt;option value=&quot;2&quot;&gt;Buenos Aires&lt;/option&gt;
    &lt;option value=&quot;3&quot;&gt;C&oacute;rdoba&lt;/option&gt;
&lt;/select&gt;</code></pre>
    <p>Si se quiere poder ordenar el select multiple, hay que usar la clase <code>.selectize-order</code>.</p>
    <div class="docs-example">
        <select class="selectize-order" multiple>
            <option value="1">Capital Federal</option>
            <option value="2">Buenos Aires</option>
            <option value="3">Córdoba</option>
        </select>
    </div>
    <pre><code>&lt;select class=&quot;selectize-order&quot; multiple&gt;
    &lt;option value=&quot;1&quot;&gt;Capital Federal&lt;/option&gt;
    &lt;option value=&quot;2&quot;&gt;Buenos Aires&lt;/option&gt;
    &lt;option value=&quot;3&quot;&gt;C&oacute;rdoba&lt;/option&gt;
&lt;/select&gt;</code></pre>
    <p>Para enlazar 2 selects hay que utilizar:</p>
    <ul>
        <li>El atributo <code>data-chained=&quot;#otroSelect&quot;</code> para especificar con que otro select está enlazado.</li>
        <li>El atributo <code>data-chained-value=&quot;2&quot;</code> para especificar que valor tiene que tener el select para habilitar el enlazado. Se puede utilizar un array <code>data-chained-value='[&quot;argentina&quot;, &quot;brasil&quot;]'</code> para especificar múltiples valores.</li>
        <li>El atributo <code>data-chained-url="https://sitio.loc/ciudades?provincia="</code> para especificar desde que URL se obtienen los <code>&lt;option&gt;</code>s para el select enlazado. Al final de la URL se agrega automáticamente el <code>value</code> del <code>option</code> seleccionado.</li>
        <li><code>data-chained-value</code> y <code>data-chained-url</code> se pueden combinar. Si no se especifica ninguno, el select enlazado se habilitará siempre y cuando el actual tenga algún valor seleccionado.</li>
    </ul>
    <div class="docs-example">
        <div class="form-group">
            <label for="pais">País (seleccionar Argentina)</label>
            <select id="pais" class="selectize" data-chained="#provincia" data-chained-value="argentina">
                <option></option>
                <option value="afghanistan">Afghanistan</option>
                <option value="argentina">Argentina</option>
                <option value="brasil">Brasil</option>
            </select>
        </div>
        <div class="form-group">
            <label for="provincia">Provincia</label>
            <select id="provincia" class="selectize" data-chained="#ciudad" data-chained-url="{{ $page->baseUrl }}/ciudades/" disabled>
                <option></option>
                <option value="capital-federal.json">Capital Federal</option>
                <option value="provincia-de-buenos-aires.json">Provincia de Buenos Aires</option>
                <option value="catamarca.json">Catamarca</option>
            </select>
        </div>
        <div class="form-group">
            <label for="ciudad">Ciudad</label>
            <select id="ciudad" class="selectize" disabled></select>
        </div>
    </div>
    <pre><code>&lt;div class=&quot;form-group&quot;&gt;
    &lt;label for=&quot;pais&quot;&gt;Pa&iacute;s (seleccionar Argentina)&lt;/label&gt;
    &lt;select id=&quot;pais&quot; class=&quot;selectize&quot; data-chained=&quot;#provincia&quot; data-chained-value=&quot;argentina&quot;&gt;
        &lt;option value=&quot;afghanistan&quot;&gt;Afghanistan&lt;/option&gt;
        &lt;option value=&quot;argentina&quot;&gt;Argentina&lt;/option&gt;
        &lt;option value=&quot;brasil&quot;&gt;Brasil&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class=&quot;form-group&quot;&gt;
    &lt;label for=&quot;provincia&quot;&gt;Provincia&lt;/label&gt;
    &lt;select id=&quot;provincia&quot; class=&quot;selectize&quot; data-chained=&quot;#ciudad&quot; data-chained-url=&quot;{{ $page->baseUrl }}/ciudades/&quot; disabled&gt;
        &lt;option&gt;&lt;/option&gt;
        &lt;option value=&quot;capital-federal.json&quot;&gt;Capital Federal&lt;/option&gt;
        &lt;option value=&quot;provincia-de-buenos-aires.json&quot;&gt;Provincia de Buenos Aires&lt;/option&gt;
        &lt;option value=&quot;catamarca.json&quot;&gt;Catamarca&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class=&quot;form-group&quot;&gt;
    &lt;label for=&quot;ciudad&quot;&gt;Ciudad&lt;/label&gt;
    &lt;select id=&quot;ciudad&quot; class=&quot;selectize&quot; disabled&gt;&lt;/select&gt;
&lt;/div&gt;</code></pre>
@endsection
