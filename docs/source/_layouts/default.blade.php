<!doctype html>
<html lang="es">
    <head>
        @include('_includes.head')
    </head>
    <body data-spy="scroll" data-target=".main-navbar">
        @include('_includes.mobile-header')
        @include('_includes.main-navbar')
        <div id="app" class="main-container">
            @include('_includes.main-header')
            <div class="page-wrapper">
                @yield('content')
            </div>
        </div>
        @include('_includes.footer')
    </body>
</html>