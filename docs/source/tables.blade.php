@extends('_layouts.default', ['title' => 'Tablas'])

@section('content')
    <h1 class="docs-title">Tablas</h1>
    <p class="docs-lead">Las tablas funcionan con <a href="https://datatables.net/" target="_blank">DataTables</a> y los estilos de <a href="https://getbootstrap.com/docs/4.1/content/tables/" target="_blank">Bootstrap</a>.</p>
    <h2 class="docs-section-title" id="datatables">DataTables</h2>
    <ul>
        <li>La clase <code>.table</code> es necesaria para el correcto funcionamiento.</li>
        <li>Usar la clase <code>.dataTable</code> para aplicar las funcionalidades de DataTables.</li>
        <li>La clase <code>.no-sort</code> en un <code>&lt;th&gt;</code> hace que esa columna no sea ordenable.</li>
        <li>La clase <code>.no-search</code> en un <code>&lt;th&gt;</code> excluye esa columna de las búsquedas.</li>
        <li>Usar la clase <code>.table-btns</code> en el <code>&lt;td&gt;</code> que contiene los botones para aplicar los estilos correspondientes.</li>
        <li>La clase <code>.table-responsive</code> se agrega automáticamente.</li>
    </ul>
    <p>Se pueden aplicar filtros (<code>&lt;select&gt;</code>s) dentro de DataTables o por fuera. Los filtros hacen uso del método <a href="https://datatables.net/reference/api/column().search()" target="_blank">column().search()</a>.</p>
    <ul>
        <li>La <code>&lt;table&gt;</code> tiene que tener un ID y <code>.has-custom-filters</code>.</li>
        <li>Usar <code>data-column-filter="2"</code> para indicar el número de la columna en la que se va a buscar (0 index).</li>
        <li>Para aplicar filtros por fuera se debe usar <code>data-table="exampleTable"</code> especificando el ID de la tabla que se quiere filtrar.</li>
        <li>Para aplicar filtros por dentro se debe crear un <code>.dataTables-custom-filters</code> con un <code>data-table</code>. Los <code>&lt;select&gt;</code>s van dentro y no deben llevar <code>data-table</code>, solo <code>data-column-filter</code>.</li>
    </ul>
    <i class="fas fa-pencil-alt" data-fa-symbol="edit"></i>
    <i class="far fa-eye" data-fa-symbol="view"></i>
    <i class="fas fa-check" data-fa-symbol="check"></i>
    <i class="fas fa-times" data-fa-symbol="times"></i>
    <div class="docs-example">
        <div class="btn-toolbar mb-3 justify-content-end">
            <div class="input-group">
                <select name="provincia" class="custom-select custom-select-sm" data-table="exampleTable" data-column-filter="4">
                    <option value="">Todas las ciudades</option>
                    <option value="Buenos Aires">Buenos Aires</option>
                    <option value="California">California</option>
                    <option value="Yeovil">Yeovil</option>
                    <option value="Wellingborough">Wellingborough</option>
                </select>
            </div>
        </div>
        <div class="dataTables-custom-filters" data-table="exampleTable">
            <select name="sexo" class="custom-select custom-select-sm" data-column-filter="2">
                <option value="">Todos los sexos</option>
                <option value="Femenino">Femenino</option>
                <option value="Masculino">Masculino</option>
            </select>
        </div>
        <table id="exampleTable" class="table table-hover dataTable has-custom-filters">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Sexo</th>
                    <th>Edad</th>
                    <th>Ciudad</th>
                    <th class="text-center">Vive</th>
                    <th class="no-sort no-search"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Thom</td>
                    <td>Yorke</td>
                    <td>Masculino</td>
                    <td>50</td>
                    <td>Wellingborough</td>
                    <td class="text-center" data-order="1">
                        <svg class="text-success svg-inline--fa fa-w-16"><use xlink:href="#check"></use></svg>
                    </td>
                    <td class="table-btns">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#edit"></use></svg>
                        </a>
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Charly</td>
                    <td>García</td>
                    <td>Masculino</td>
                    <td>67</td>
                    <td>Buenos Aires</td>
                    <td class="text-center" data-order="1">
                        <svg class="text-success svg-inline--fa fa-w-16"><use xlink:href="#check"></use></svg>
                    </td>
                    <td class="table-btns">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#edit"></use></svg>
                        </a>
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>María Gabriela</td>
                    <td>Epumer</td>
                    <td>Femenino</td>
                    <td>39</td>
                    <td>Buenos Aires</td>
                    <td class="text-center" data-order="0">
                        <svg class="text-danger svg-inline--fa fa-w-16"><use xlink:href="#times"></use></svg>
                    </td>
                    <td class="table-btns">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#edit"></use></svg>
                        </a>
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>PJ</td>
                    <td>Harvey</td>
                    <td>Femenino</td>
                    <td>49</td>
                    <td>Yeovil</td>
                    <td class="text-center" data-order="1">
                        <svg class="text-success svg-inline--fa fa-w-16"><use xlink:href="#check"></use></svg>
                    </td>
                    <td class="table-btns">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#edit"></use></svg>
                        </a>
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Jeff</td>
                    <td>Buckley</td>
                    <td>Masculino</td>
                    <td>30</td>
                    <td>California</td>
                    <td class="text-center" data-order="0">
                        <svg class="text-danger svg-inline--fa fa-w-16"><use xlink:href="#times"></use></svg>
                    </td>
                    <td class="table-btns">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#edit"></use></svg>
                        </a>
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver">
                            <svg class="svg-inline--fa fa-w-16"><use xlink:href="#view"></use></svg>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <pre><code>&lt;select name=&quot;provincia&quot; class=&quot;custom-select custom-select-sm&quot; data-table=&quot;exampleTable&quot; data-column-filter=&quot;4&quot;&gt;
    &lt;option value=&quot;&quot;&gt;Todas las ciudades&lt;/option&gt;
    &lt;option value=&quot;Buenos Aires&quot;&gt;Buenos Aires&lt;/option&gt;
    &lt;option value=&quot;California&quot;&gt;California&lt;/option&gt;
    &lt;option value=&quot;Yeovil&quot;&gt;Yeovil&lt;/option&gt;
    &lt;option value=&quot;Wellingborough&quot;&gt;Wellingborough&lt;/option&gt;
&lt;/select&gt;
&lt;div class=&quot;dataTables-custom-filters&quot; data-table=&quot;exampleTable&quot;&gt;
    &lt;select name=&quot;sexo&quot; class=&quot;custom-select custom-select-sm&quot; data-column-filter=&quot;2&quot;&gt;
        &lt;option value=&quot;&quot;&gt;Todos los sexos&lt;/option&gt;
        &lt;option value=&quot;Femenino&quot;&gt;Femenino&lt;/option&gt;
        &lt;option value=&quot;Masculino&quot;&gt;Masculino&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;table id=&quot;exampleTable&quot; class=&quot;table table-hover dataTable has-custom-filters&quot;&gt;
    &lt;thead&gt;
        &lt;tr&gt;
            &lt;th&gt;Nombre&lt;/th&gt;
            &lt;th&gt;Apellido&lt;/th&gt;
            &lt;th&gt;Sexo&lt;/th&gt;
            &lt;th&gt;Edad&lt;/th&gt;
            &lt;th&gt;Ciudad&lt;/th&gt;
            &lt;th class=&quot;text-center&quot;&gt;Vive&lt;/th&gt;
            &lt;th class=&quot;no-sort no-search&quot;&gt;&lt;/th&gt;
        &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td&gt;Thom&lt;/td&gt;
            &lt;td&gt;Yorke&lt;/td&gt;
            &lt;td&gt;Masculino&lt;/td&gt;
            &lt;td&gt;50&lt;/td&gt;
            &lt;td&gt;Wellingborough&lt;/td&gt;
            &lt;td class=&quot;text-center&quot; data-order=&quot;1&quot;&gt;
                &lt;svg class=&quot;text-success svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#check&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
            &lt;/td&gt;
            &lt;td class=&quot;table-btns&quot;&gt;
                &lt;a href=&quot;#&quot; class=&quot;btn btn-sm btn-primary&quot; data-toggle=&quot;tooltip&quot; title=&quot;Editar&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#edit&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/a&gt;
                &lt;a href=&quot;#&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Ver&quot;&gt;
                    &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                &lt;/a&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;</code></pre>
    <h2 class="docs-section-title" id="ajax-pagination">AJAX pagination</h2>
    <p>Para tablas estáticas (o cualquier tipo de paginado) se puede habilitar un paginado mediante AJAX. Hay que usar la clase <code>.ajax-pagination</code> en el <code>&lt;div&gt;</code> que engloba la tabla y el <code>.pagination</code>.</p>
    <pre><code>&lt;div class=&quot;ajax-pagination&quot;&gt;
    &lt;div class=&quot;table-responsive&quot;&gt;
        &lt;table class=&quot;table&quot;&gt;
            &lt;thead&gt;
                &lt;tr&gt;
                    &lt;th&gt;Postulado&lt;/th&gt;
                    &lt;th&gt;REF&lt;/th&gt;
                    &lt;th&gt;Puesto&lt;/th&gt;
                    &lt;th&gt;Comentarios&lt;/th&gt;
                    &lt;th&gt;&lt;/th&gt;
                &lt;/tr&gt;
            &lt;/thead&gt;
            &lt;tbody&gt;
                &lt;tr&gt;
                    &lt;td&gt;28/08/2018&lt;/td&gt;
                    &lt;td&gt;TR02&lt;/td&gt;
                    &lt;td&gt;Desarrollador Back End Senior, CABA&lt;/td&gt;
                    &lt;td&gt;Comentario Sobre El Trabajo&lt;/td&gt;
                    &lt;td class=&quot;table-btns&quot;&gt;
                        &lt;a href=&quot;http://getit.loc/admin/jobs/30131719/edit&quot; class=&quot;btn btn-sm btn-primary&quot; data-toggle=&quot;tooltip&quot; title=&quot;Editar&quot;&gt;
                            &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#edit&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                        &lt;/a&gt;
                        &lt;a href=&quot;http://getit.loc/admin/jobs/30131719/desarrollador-back-end-senior&quot; class=&quot;btn btn-sm btn-info&quot; data-toggle=&quot;tooltip&quot; title=&quot;Ver&quot;&gt;
                            &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#view&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                        &lt;/a&gt;
                        &lt;a href=&quot;http://getit.loc/busquedas/30131719/desarrollador-back-end-senior&quot; class=&quot;btn btn-sm btn-secondary&quot; data-toggle=&quot;tooltip&quot; title=&quot;Ir&quot; target=&quot;_blank&quot;&gt;
                            &lt;svg class=&quot;svg-inline--fa fa-w-16&quot;&gt;&lt;use xlink:href=&quot;#go&quot;&gt;&lt;/use&gt;&lt;/svg&gt;
                        &lt;/a&gt;
                    &lt;/td&gt;
                &lt;/tr&gt;
            &lt;/tbody&gt;
        &lt;/table&gt;
    &lt;/div&gt;
    &lt;ul class=&quot;pagination&quot;&gt;
        &lt;li class=&quot;page-item disabled&quot;&gt;&lt;span class=&quot;page-link&quot;&gt;&amp;laquo;&lt;/span&gt;&lt;/li&gt;
        &lt;li class=&quot;page-item active&quot;&gt;&lt;span class=&quot;page-link&quot;&gt;1&lt;/span&gt;&lt;/li&gt;
        &lt;li class=&quot;page-item&quot;&gt;&lt;a class=&quot;page-link&quot; href=&quot;#&quot;&gt;2&lt;/a&gt;&lt;/li&gt;
        &lt;li class=&quot;page-item&quot;&gt;&lt;a class=&quot;page-link&quot; href=&quot;#&quot; rel=&quot;next&quot;&gt;&amp;raquo;&lt;/a&gt;&lt;/li&gt;
    &lt;/ul&gt;
&lt;/div&gt;</code></pre>
@endsection
