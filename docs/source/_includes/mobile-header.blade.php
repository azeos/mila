<header class="mobile-header navbar navbar-expand navbar-dark bg-dark fixed-top">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <button type="button" class="btn btn-link nav-link main-navbar-toggler">
                <i class="fas fa-bars"></i>
            </button>
        </li>
        <li class="nav-item">
            <a href="{{ $page->baseUrl }}/" class="nav-link">{{ $page->title }}</a>
        </li>
    </ul>
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown">
                <i class="far fa-ellipsis-v"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item" target="_blank">Ver sitio</a>
                <a href="#" class="dropdown-item">Salir</a>
            </div>
        </li>
    </ul>
</header>