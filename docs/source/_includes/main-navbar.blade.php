<nav class="main-navbar">
    <div class="head">
        <a href="{{ $page->baseUrl }}/" class="logo">
            {{ $page->title }}
        </a>
    </div>
    <ul>
        <li class="section">Empezar</li>
        <li class="{{ $page->getPath() ? '' : 'active' }}">
            <a href="{{ $page->baseUrl }}/"><i class="fas fa-fw fa-code"></i> Introducción</a>
            <ul>
                <li>
                    <a href="#instalacion" class="nav-link">Instalación</a>
                </li>
                <li>
                    <a href="#plantilla" class="nav-link">Plantilla</a>
                </li>
                <li>
                    <a href="#dependencias" class="nav-link">Dependencias</a>
                </li>
                <li>
                    <a href="#plugins" class="nav-link">Plugins</a>
                </li>
                <li>
                    <a href="#fonts" class="nav-link">Fonts</a>
                </li>
            </ul>
        </li>
        <li class="{{ $page->active('layout') }}">
            <a href="{{ $page->baseUrl }}/layout"><i class="fas fa-fw fa-browser"></i> Layout</a>
            <ul>
                <li>
                    <a href="#mobile-header" class="nav-link">Mobile header</a>
                </li>
                <li>
                    <a href="#main-header" class="nav-link">Main header</a>
                </li>
                <li>
                    <a href="#main-navbar" class="nav-link">Main navbar</a>
                </li>
                <li>
                    <a href="#page-content" class="nav-link">Page content</a>
                </li>
            </ul>
        </li>
        <li class="section">Componentes</li>
        <li>
            <a href="#"><i class="fab fa-fw fa-font-awesome-flag"></i> Font Awesome</a>
        </li>
        <li class="{{ $page->active('tables') }}">
            <a href="{{ $page->baseUrl }}/tables"><i class="fal fa-fw fa-table"></i> Tablas</a>
            <ul>
                <li>
                    <a href="#datatables" class="nav-link">DataTables</a>
                </li>
                <li>
                    <a href="#ajax-pagination" class="nav-link">AJAX pagination</a>
                </li>
            </ul>
        </li>
        <li class="{{ $page->active('forms') }}">
            <a href="{{ $page->baseUrl }}/forms"><i class="fas fa-fw fa-edit"></i> Formularios</a>
            <ul>
                <li>
                    <a href="#stateful-button" class="nav-link">Stateful Button</a>
                </li>
                <li>
                    <a href="#form-ajax" class="nav-link">Form AJAX</a>
                </li>
                <li>
                    <a href="#bootbox" class="nav-link">Bootbox</a>
                </li>
                <li>
                    <a href="#dropzonejs" class="nav-link">DropzoneJS</a>
                </li>
                <li>
                    <a href="#easymde" class="nav-link">EasyMDE</a>
                </li>
                <li>
                    <a href="#flatpickr" class="nav-link">flatpickr</a>
                </li>
                <li>
                    <a href="#holderjs" class="nav-link">Holder.js</a>
                </li>
                <li>
                    <a href="#jasny-bootstrap" class="nav-link">Jasny Bootstrap</a>
                </li>
                <li>
                    <a href="#jquery-ui" class="nav-link">jQuery UI</a>
                </li>
                <li>
                    <a href="#selectizejs" class="nav-link">Selectize.js</a>
                </li>
            </ul>
        </li>
        <li class="section">Ejemplos</li>
        <li>
            <a href="#"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard</a>
        </li>
        <li>
            <a href="#"><i class="fas fa-fw fa-users"></i> Perfiles (index)</a>
        </li>
        <li>
            <a href="#"><i class="fas fa-fw fa-user"></i> Perfil (show)</a>
        </li>
        <li>
            <a href="#"><i class="fas fa-fw fa-user-edit"></i> Perfil (edit)</a>
        </li>
        <li class="mb-auto">
            <a href="#"><i class="fas fa-fw fa-sort"></i> Ordenar</a>
        </li>
        <li>
            <a href="https://gitlab.com/azeos/mila" target="_blank"><i class="fab fa-fw fa-gitlab"></i> v1.0.7</a>
        </li>
    </ul>
</nav>
