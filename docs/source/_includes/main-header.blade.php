<header class="main-header navbar navbar-expand navbar-light">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <button type="button" class="btn btn-link nav-link main-navbar-toggler">
                <i class="fas fa-bars"></i>
            </button>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" target="_blank">
                <i class="far fa-eye"></i>
                Ver sitio
            </a>
        </li>
    </ul>
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="{{ $page->baseUrl }}/assets/images/user-circle.svg" class="rounded-circle" alt="Avatar" height="40">
                Usuario
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item">Salir</a>
            </div>
        </li>
    </ul>
</header>