<?php

return [
    'baseUrl'     => '',
    'production'  => false,
    'collections' => [],
    'title'       => 'Mila',
    'active'      => function ($page, $section) {
        return str_contains($page->getPath(), $section) ? 'active' : '';
    },
    'locales'     => [
        'es' => 'Español',
        'en' => 'Inglés',
    ],
];
