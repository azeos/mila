# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/azeos/mila/compare/v1.0.7...master)

## [1.0.7](https://gitlab.com/azeos/mila/tags/v1.0.7) - 2020-03-28
### Fixed
- El "maxFiles" de Dropzone.js ahora tiene en cuenta los archivos precargados.

## [1.0.6](https://gitlab.com/azeos/mila/tags/v1.0.6) - 2020-03-13
### Changed
- Se actualizó selectize-bootstrap-4-style al commit 451ba39 del 2020-02-22.

### Fixed
- Ahora funciona el touch sobre los "selectize", había un problema con el cierre del navbar con swipe.

## [1.0.5](https://gitlab.com/azeos/mila/tags/v1.0.5) - 2020-03-05
### Added
- Trigger "main-navbar.opened".
- Trigger "main-navbar.closed".
- Se expuso la opción "maxFiles" de Dropzone.js.

### Changed
- Se actualizó DataTables a la v1.10.20.
- Se actualizó Dropzone.js a la v5.7.0
- Se reemplazó en DataTables el uso del plugin "diacritics" por "accent".
- Bindeados los filtros de DataTables al body.

## [1.0.4](https://gitlab.com/azeos/mila/tags/v1.0.4) - 2019-07-25
### Added
- .main-navbar-overlay para cambiar el estilo en el que se abre el menú.

### Changed
- .modal-remote on('hidden.bs.modal') movido al body.
- Datepickers movidos al body.
- .ajax-pagination movido al body.
- Se fuerza un reload si se cambia el window.location en un .form-ajax.
- .form-ajax, ahora primero se ejecuta el "buttonState()" y después el custom trigger "form.ajax.success".

### Removed
- Se eliminarion variables de SCSS que no se estaban utilizando.

## [1.0.3](https://gitlab.com/azeos/mila/tags/v1.0.3) - 2019-03-21
### Fixed
- Solo se cierran automáticamente los alerts con la clase ".fade".

## [1.0.2](https://gitlab.com/azeos/mila/tags/v1.0.2) - 2019-03-21
### Added
- Distintas opciones para cargar contenido remoto en un modal (#13).
- Datepickers linked (#11).
- Datetimepickers (#11).

### Fixed
- Main-navbar z-index (#12).
- Bootbox z-index (#14).

## [1.0.1](https://gitlab.com/azeos/mila/tags/v1.0.1) - 2019-01-25
### Removed
- Font Awesome 5 Pro Dependency

### Changed
- En la documentación se reemplazó FA5 Pro por el free CDN.

## [1.0.0](https://gitlab.com/azeos/mila/tags/v1.0.0) - 2019-01-25
### Added
- Primera versión.